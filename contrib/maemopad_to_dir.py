#!/usr/bin/python
# maemopad_to_dir.py: convert a maemopad+ memos file into
# a dir structure with PNGs and HTML
#
# (c) 2008 Jon Dowland <jon@alcopop.org>
# 
# This software is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2.1 of
# the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
# 02110-1301 USA

from sys import argv, exit
import os
from tempfile import mkdtemp, mkstemp
from shutil import move
from pysqlite2 import dbapi2 as sqlite

if len(argv) < 3:
    print "usage: maemopad_to_dir.py infile outname"
    exit()

infile = argv[1]
outname = argv[2]

conn = sqlite.connect(infile)
cursor = conn.cursor()
sql = "SELECT nodeid, parent, nameblob, bodyblob, name FROM nodes"
cursor.execute(sql)

# infile seems ok, so we can create outname
os.mkdir(outname)
tmpdir = mkdtemp(dir=outname)

# loop through rows and
    # build a list of attribute-tuples
    # save the PNGs to temporary files
    # keep a mapping of notes -> temporary file paths
temp = []
paths = {}
for id,pid,nameblob,bodyblob,name in cursor:
    temp.append((id,pid,name))
    paths[id] = {}
    if nameblob:
        fd,n = mkstemp(dir=tmpdir,suffix=".png")
        os.write(fd, nameblob)
        os.close(fd)
        paths[id]['name'] = n
    if bodyblob:
        fd,n = mkstemp(dir=tmpdir,suffix=".png")
        os.write(fd, bodyblob)
        os.close(fd)
        paths[id]['body'] = n

# build a tree
# hash as a quick-index into the tree
root = { 'id': 0, 'children': [], 'path': '.', 'name' : 'root' }
hash = { 0: root }

while len(temp) > 0:
    row = temp.pop(0)
    id,pid,name = row

    if hash.has_key(pid):
        path = "%s/%d" % (hash[pid]['path'], id)
        node = { 'id': id , 'children': [], 'path': path , 'name':name }
        hash[pid]['children'].append(node)
        hash[id] = node
        os.mkdir(("%s/%s" % (outname, node['path'])))
    else:
        temp.append(row)

# we can do away with root now, probably
if len(root['children']) == 1:
    root = root['children'][0]

# now move the paths around
while len(paths) > 0:
    key = paths.keys()[0]
    src = paths[key]
    node = hash[key]
    if src.has_key('body'):
        move(src['body'], "%s/%s/body.png" % (outname, node['path']))
    if src.has_key('name'):
        move(src['name'], "%s/%s/name.png" % (outname, node['path']))
    del paths[key]

os.rmdir(tmpdir)

# generate the HTML

sidebar_pre = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
   "http://www.w3.org/TR/html4/frameset.dtd">
<html>
  <head><title></title></head>
  <body>
'''
sidebarf = open(("%s/sidebar.html"%outname), "w")
sidebarf.write(sidebar_pre)

def node_to_html(node):
    # TODO: identify when there's no name.png (not recorded yet?)
    name = node['name'] or 'None'
    img = ''.join(['<img border="0" src="',
        node['path'], '/name.png" alt="', name, '" />'])
    href = node['path'] + '/body.png'

    ret = ''.join(['<a target="mymain" href="', href, '">', img, "</a>"])
    
    if len(node['children']) > 0:
        ret = ret + '<ul>'
        for child in node['children']:
            tmp = node_to_html(child)
            ret = ''.join([ret, "<li>", node_to_html(child), "</li>"])
        ret = ret + '</ul>'
    
    return ret

sidebarf.write('<ul><li>'+node_to_html(root)+"</li></ul>\n")


sidebar_post = '''  </body>
</html>'''
sidebarf.write(sidebar_post)
sidebarf.close

# finally, the index page
frontpage = hash[1]['path'] + '/body.png'
index = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
   "http://www.w3.org/TR/html4/frameset.dtd">
  <head><title>foo</title></head>
    <FRAMESET cols="20%%,80%%">
      <FRAME scrolling="yes" name="sidebar" src="sidebar.html" />
      <FRAME scrolling="yes" name="mymain" src="%s" />
    </FRAMESET>
    <noframes>
        ?
    </noframes>
</html>''' % frontpage
indexf = open(("%s/index.html"%outname), "w")
indexf.write(index)
indexf.close

