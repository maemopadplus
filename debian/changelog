maemopadplus (0.37) diablo; urgency=low

  * Fix a segfault for "My first memo" renames (use g_strdup)
    Thanks to silvermountain on TMO for the bug report

 -- Thomas Perl <thp@thpinfo.com>  Sun, 19 Sep 2010 11:20:26 +0200

maemopadplus (0.36) diablo; urgency=low

  * Change section from "user/accessories" to "user/office"
    (see http://wiki.maemo.org/Task:Package_categories)

 -- Thomas Perl <thp@thpinfo.com>  Sat, 20 Feb 2010 13:27:33 -0800

maemopadplus (0.35) diablo; urgency=low

  * The "Long Time No See" release

  [Thomas Perl]
  * Support for full-text searching with toolbar
  * Enable display of tree view lines
  * Updated German translation
  * Remove Debian revision number

  [Kemal Hadimli]
  * Added Russian translation by Sergei Ivanov

 -- Thomas Perl <thp@thpinfo.com>  Sat, 20 Feb 2010 06:51:20 -0800

maemopadplus (0.34-1) diablo; urgency=low

  * Update XB-Maemo-Icon-26 field in debian/control

 -- Thomas Perl <thp@perli.net>  Thu, 09 Oct 2008 11:20:46 +0200

maemopadplus (0.34-0) diablo; urgency=low

  * Remove orphaned checklist items when nodes are removed
  * Redesigned "Add new memo" dialog, better usability
  * Current date/time as default name for new memos
  * Removed legacy support code for old Hildon versions
  * Full multi-item copy and paste support for checklist items
  * Fresh Maemopad+ icons by thp

 -- Thomas Perl <thp@perli.net>  Thu, 09 Oct 2008 11:20:26 +0200

maemopadplus (0.33-1) diablo; urgency=low

  * Support wpeditor0 / wpeditor-dev instead of relying on libwpeditor-plus
  * Change Depends and Build-Depends accordingly

 -- Thomas Perl <thp@perli.net>  Tue, 30 Sep 2008 23:37:34 +0200

maemopadplus (0.33-0) diablo; urgency=low

  * Renaming notes now finally works correctly (prefill entry)
  * Disallow renaming notes with sketch names
  * Allow paste of plain text in checklist memo type
  * Use stock GTK AboutDialog instead of hand-crafted one

 -- Thomas Perl <thp@perli.net>  Wed, 24 Sep 2008 12:33:36 +0200

maemopadplus (0.32-test3) unstable; urgency=low

  * Don't insert default text when adding checklist items
  * Fix po/POTFILES.in so that maemopadplus.pot can be generated
  * Added German (de_DE) translation

 -- Thomas Perl <thp@perli.net>  Mon,  4 Feb 2008 17:44:14 +0100

maemopadplus (0.32-test2) unstable; urgency=low

  Chinook/Bora/Gregale unified test release.
  Get that Color option in the menu working again.
  Add Finnish translation from Marko Vertainen

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Fri, 20 Nov 2007 06:30:00 +0300

maemopadplus (0.32-test1) unstable; urgency=low

  Chinook test release.

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Sat, 20 Oct 2007 06:00:00 +0300

maemopadplus (0.32) unstable; urgency=low

  checklist support!

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 3 Mar 2007 07:30:00 +0300

maemopadplus (0.31) unstable; urgency=low

  closed bug#507 (empty sketches don't get saved)
  closed bug#498 (drawing the 1st line/ellipse/rectangle starts from top-left corner)
  fixed some isEdited problems with sketches
  changing brush color depending on pressure
  added pressure color sensitivity toggleitem to menu

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 25 Feb 2007 00:00:00 +0300

maemopadplus (0.30) unstable; urgency=low

  added formatted text support
  added bold/italic/underline/bulleted icons to toolbar
  added undo-redo to text editing
  font dialog now works
  changed some of the toolbar icons to hildon (qgn) counterparts
  default font size changed to 16pt
  added word-wrap to tools menu
  export now exports as html
  turned off sqlite3 sync to gain (incredible!) speed

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 20 Feb 2007 01:00:00 +0300

maemopadplus (0.25b) unstable; urgency=low

  rocker key now toggles toolbar
  added "finger" callback to sketchwidget, though it's unused for now
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 12 Feb 2007 03:40:00 +0300

maemopadplus (0.25a) unstable; urgency=low

  fixed accidental drawing on pane movage
  fixed intltool version check in configure.in
  updated the .po file
  made code prettier
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 12 Feb 2007 02:30:00 +0300

maemopadplus (0.25) unstable; urgency=low

  added simple range check for sketchwidget, prevents hangs on bora
  added basic pressure support on bora (with ugly hacks)
  made the eraser bigger
  fixed filechange logic
  fullscreen (without toolbars/tree) now also removes scrollbars
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 10 Feb 2007 01:53:00 +0300

maemopadplus (0.24a) unstable; urgency=low

  hopefully fixed icon to debian/control
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 09 Feb 2007 10:41:00 +0300

maemopadplus (0.24) unstable; urgency=low

  replaced fullscreen menu with view menu
  fullscreen key now just toggles fullscreen, doesn't touch toolbar
  toggle treeview button added to toolbar (esc key also works)
  creating a new file now resets view flags, pane position, and adds a "my first memo" (sketchnode) and saves
  added icon to debian/control
  removed some of the gutters, still needs work
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 09 Feb 2007 07:47:00 +0300

  first build on bora
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 08 Feb 2007 03:42:00 +0300

maemopadplus (0.23a) unstable; urgency=low

  fixed sqlite3 dependency
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 26 Jan 2007 18:30:00 +0300

maemopadplus (0.23) unstable; urgency=low

  "nodes" are now "memos"
  brand new treeview selection logic in place
  improved about dialog, added license info and visit-homepage button
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 16 Jun 2006 03:10:00 +0300

maemopadplus (0.22-internal0) unstable; urgency=low

  added line drawing option
  changed homepage url
  sketch bottom-right parts are now cropped for fast loading (switchnode-saving got slower but switchnode-loading got faster)
  added squared line drawing
  made treeview expanders work
  "new sketch node" / "new text node" menuitems unified into one "new node" menuitem
    
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 15 Jun 2006 02:44:00 +0300

maemopadplus (0.22) unstable; urgency=low

  fixed: added maemo-select-menu-location to pre-depends
  saving brushsize and brushcolor
  renamed osso service name to org.maemo.maemopadplus, then renamed it back
  added export node option
  added shape support (up/down/left/right keys for filled/unfilled and squared modes)
  made demo screenshots
  
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 13 Jun 2006 21:45:00 +0300

maemopadplus (0.21) unstable; urgency=low

  fixed: closing via the "X" button really closes the app now
  added: maemo-select-menu-location to postinst
  fixed: x-osso-service
  fixed: confirmation dialogs
  fixed: hardware keys
  watched alien vs. predator
  
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 11 Jun 2006 04:17:00 +0300

maemopadplus (0.20) unstable; urgency=low

  ported to IT2006
  fixed maemopad_yes / maemopad_no strings left over from the original maemopad
  added simple dbus (why?)
  
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 10 Jun 2006 23:13:00 +0300

maemopadplus (0.18a) unstable; urgency=low

  fixed serious memleak on pixbuf loading
  fixed license to be LGPL again (because we forked from maemopad)
  replaced a tab char in changelog
  added todo to makefile.am
  
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 5 Apr 2006 17:50:00 +0200

maemopadplus (0.18) unstable; urgency=low

  project home moved to sf.net
  fixed the "clicking on current sketch erases all work" bug
  added "Clear" item to edit menu
  now also saving fullScreen status in the misc table
  fixed redo -- could be optimized by checking for dupe tilepixmaps in adjacent revisions and refcounting
  made graph squares bigger (looks better)
  rearranged brushsize button with the eraser toggle
  if the brushsize button is clicked when eraser is on, it just turns off eraser and won't pop menu
  made the eraser smaller (2x +4 of the current brush size, 3x+4 was too large)
  added graph background to the sketchnodename dialog (the borders don't work anymore, to compensate)
  treeview loads up with node0 selected. ugly workaround in place (mainview->loading)

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 5 Apr 2006 03:10:00 +0200

  added watch cursor as a "loading" sign
  added fullscreen submenu to menu/toolbar
  made the eraser larger (3x +4 of the current brush size)

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 3 Apr 2006 23:53:00 +0200

  now rendering the background pixmap from tiles and precalcing them at widget init (just to test the performance)
  implemented primitive(=no layering/selection) clipboard functionality for sketches
  extended toolbar to the treeview

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 3 Apr 2006 01:13:00 +0200

  added a checkbox in create node dialog to create the new node as sibling/child
  that checkbox's state is saved to db
  made undo/redo buttons change sensitivity
  made default newnodetype sketch
  moved node operations in the menu to a node submenu
  made tools menu not appear when no node is selected
  made [-]/[+] also work in the create node dialog

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 2 Apr 2006 19:42:00 +0200

  added borders around the main sketchwidget
  fixed graph bg alignment
  tried 8bpp, didn't work
  made drawing faster
  implemented undo/redo on sketches
  [-]/[+] keys do undo/redo on sketches now
  
  
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 1 Apr 2006 00:19:00 +0200

maemopadplus (0.17) unstable; urgency=low

  shamelessly stole and modified icon from the tango project (for the lines/squares toolbutton)
  fixed first sketchload always failing bug
  added OSSO event handler for saving unsaved data on request
  added the new menus and font/color selection to app menu (under tools)
  added todo
  
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 31 Mar 2006 04:40:00 +0200

  added notebook lines/squares
  moved brushsize buttons to a brushsize icon and menu
  shamelessly stole eraser/pencil pixmaps from xournal
  eraser size is now 4 points larger than the current brushsize
  the toolbar icon and the menu reflects eraser size

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 30 Mar 2006 23:20:00 +0200

  added a flags column to data table
  added a dataVersion key to the misc table
  wrote dirty code to migrate tables from v0 to v1

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 24 Mar 2006 18:20:00 +0200

  added border to sketchname area
  sketchname area enlarged to 288x96
  contents of the sketchname area are resized (and centered) if necessary

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 20 Mar 2006 01:32:00 +0200

maemopadplus (0.16) unstable; urgency=low

  added brush size buttons (no icons yet)
  added eraser button
  save changes dialog now has a cancel button
  added hildoncolorbutton to toolbar
  "new" button changed icon
  if a sketch node is selected, cut/copy/paste/font buttons and edit menu are hidden
  added tap-and-hold menu to the tree

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 19 Mar 2006 05:13:00 +0200

  now got 5 fullscreen modes (0:normal 1:notree-normal 2:normal-full 3:notree-full 4:justdoc-full)
  removed "open file" button from the toolbar
  "new" button on the toolbar now creates a new node (same type as the current node)

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 16 Mar 2006 17:35:00 +0200

maemopadplus (0.15) unstable; urgency=low

  upgraded gtkctree to gtktreeview
  made the sketch stuff (gtkdrawingarea) into a SketchWidget class
  now using sqlite instead of xml
  sketches are saved
  if no document (well, datafile) loaded, starts out with a "New Document"
  added about box
  added adjustable ink color
  strings i18n'ized (need to generate .pot)
  4 fullscreen modes
  ESC key exits fullscreen or closes app
  added option to make node names as sketches
 
 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 14 Mar 2006 19:00:00 +0200

maemopadplus (0.1) unstable; urgency=low

  initial (test) release. code forked from maemopad (1.4)

 -- Kemal 'disq' Hadimli <disqkk@gmail.com>  Thu, 9 Mar 2006 12:50:00 +0200
