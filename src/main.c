/*
 * This file is part of maemopad+
 *
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <ui/interface.h>
#include <ui/callbacks.h>
#include <string.h>

#include <gtk/gtkmain.h>
#include <hildon/hildon-program.h>
#include <hildon/hildon-note.h>
#include <libintl.h>
#include <locale.h>
#include <libosso.h>

#define _(String) gettext(String)

#include <config.h>
#include <appdata.h>

gint
dbus_callback (const gchar *interface, const gchar *method,
               GArray *arguments, gpointer data,
               osso_rpc_t *retval)
{
  printf ("maemopad+ dbus: %s, %s\n", interface, method);

  if (!strcmp (method, "top_application"))
      gtk_window_present (GTK_WINDOW (data));

  retval->type = DBUS_TYPE_INVALID;
  return OSSO_OK;
}

int main(int argc, char *argv[])
{
	AppData *data;
	HildonProgram *app;
	MainView *main_view;

	/* Initialise the locale stuff */
	setlocale(LC_ALL, "");
	bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
	textdomain(GETTEXT_PACKAGE);

	/* Init the gtk - must be called before any hildon stuff */
	gtk_init(&argc, &argv);

  /* Create the hildon application and setup the title */
  app = HILDON_PROGRAM ( hildon_program_get_instance () );
  g_set_application_name ( _("Maemopad+") );

	/* Create the data and views for our application */
	data = g_new0(AppData, 1);
	data->osso = osso_initialize("maemopadplus", VERSION, FALSE, NULL);
	g_assert(data->osso);
	data->app = app;
	main_view = interface_main_view_new(data);
	hildon_program_add_window( data->app, data->main_view );

	osso_return_t ossores;

	/* Add handler for Exit D-BUS messages */
	ossores = osso_application_set_exit_cb(data->osso, exit_event_handler, (gpointer) main_view);
	if (ossores != OSSO_OK)
		{
			g_print("Error setting exit callback (%d)\n", ossores);
			return OSSO_ERROR;
		}

	/* Add handler for hardware D-BUS messages */
	ossores = osso_hw_set_event_cb(data->osso, NULL, hw_event_handler, (gpointer) main_view);
	if (ossores != OSSO_OK)
		{
			g_print("Error setting HW state callback (%d)\n", ossores);
			return OSSO_ERROR;
		}

	ossores = osso_rpc_set_default_cb_f (data->osso, dbus_callback, data->main_view);
	if (ossores != OSSO_OK)
	    {
	      fprintf (stderr, "osso_rpc_set_default_cb_f failed: %d.\n", ossores);
	      return 1;
	    }

	data->gconf = gconf_client_get_default();
	gchar *gcfilename = gconf_client_get_string(data->gconf, "/apps/maemopadPlus/general/lastdocument", NULL);

	gtk_widget_show(GTK_WIDGET(data->main_view));

	gtk_widget_hide(main_view->scrolledwindow);

	gtk_widget_show(sketchwidget_get_mainwidget(main_view->sk));
	gtk_widget_realize(sketchwidget_get_drawingarea(main_view->sk));

/*	gtk_widget_map(sketchwidget_get_drawingarea(main_view->sk));
*/
	gtk_widget_hide(sketchwidget_get_mainwidget(main_view->sk));

	if (gcfilename)
		{
			open_file(gcfilename, main_view);
			g_free(gcfilename);
		}

	do
		{
			do												/*FIXME:bad code */
				{
					if (main_view->file_name == NULL)
						{
							if (interface_create_new_file(main_view) == CONFRESP_YES)
								callback_file_new(NULL, main_view);
							else
								callback_file_open(NULL, main_view);
						}
					if (main_view->file_name == NULL)
						{
							HildonNote *hn = NULL;
							gint response;

							hn = HILDON_NOTE(hildon_note_new_confirmation_add_buttons(GTK_WINDOW(main_view->data->main_view), _("Exit?"), _("Yes"), CONFRESP_YES, _("No"), CONFRESP_NO, NULL, NULL));
							response = gtk_dialog_run(GTK_DIALOG(hn));
							gtk_widget_destroy(GTK_WIDGET(hn));
							if (response == TRUE)
								break;
						}
			} while(main_view->file_name == NULL);
			if (main_view->file_name == NULL)
				break;

			/* Begin the main app */
			gtk_widget_show(GTK_WIDGET(data->main_view));
			gtk_main();

			if (main_view->file_name)
				{
					printf("exit:*%s*\n", main_view->file_name);
					gconf_client_set_string(data->gconf, "/apps/maemopadPlus/general/lastdocument", main_view->file_name, NULL);
				}
	} while(FALSE);

	/* Clean up */
	if (main_view->db)
		sqlite3_close(main_view->db);

	sketchwidget_destroy(main_view->sk);
	interface_main_view_destroy(main_view);

	osso_deinitialize(data->osso);
	g_free(data);

	return 0;
}
