/*
 * This file is part of maemopad+
 *
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <ui/interface.h>
#include <ui/callbacks.h>
#include <ui/sketchwidget.h>
#include <gtk/gtk.h>
#include <libintl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <config.h>

#include <hildon/hildon-program.h> 
#include <hildon/hildon-file-chooser-dialog.h>
#include <hildon/hildon-note.h>
#include <hildon/hildon-font-selection-dialog.h>
#include <hildon/hildon-color-button.h>

/*
 * Privates: 
 */

GtkWidget *create_brushsizemenu(MainView * main);
GtkWidget *create_viewmenu(MainView * main);
GtkWidget *create_shapemenu(MainView * main);
GtkWidget *create_sketchlinesmenu(MainView * main);
GtkWidget *create_menu(MainView * main);
static void create_toolbar(MainView * main);
void create_textarea(MainView * main);
void create_treeview(MainView * main);
static void _tool_button_set_icon_name(GtkToolButton *button, const gchar *icon_name);
void create_checklist(MainView * main);

void sk_set_brushsize(MainView * main, guint bsize)
{
	int i = (bsize / 2) - 1;

	if (i >= BRUSHSIZE_COUNT)
		i = BRUSHSIZE_COUNT - 1;
	if (i < 0)
		i = 0;

	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(main->brushsizemenuitems[i]), FALSE);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(main->brushsizemenuitems[i]), TRUE);
}

static void _tool_button_set_icon_name(GtkToolButton *button, const gchar *icon_name)
{
	GtkWidget *image;
	image = gtk_image_new_from_icon_name(icon_name, GTK_ICON_SIZE_SMALL_TOOLBAR);
	gtk_tool_button_set_icon_widget(button, image);
}

void _toggle_tool_button_set_inconsistent(GtkToggleToolButton *button, gboolean inconsistent)
{
GtkWidget *toggle_button = gtk_bin_get_child(GTK_BIN(button));
gtk_toggle_button_set_inconsistent(GTK_TOGGLE_BUTTON(toggle_button), inconsistent);
}

MainView *interface_main_view_new(AppData * data)
{
	MainView *result = g_new0(MainView, 1);

	GtkWidget *main_vbox = gtk_vbox_new(FALSE, 0);

	result->hpaned = gtk_hpaned_new();

	result->clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
	gtk_clipboard_set_can_store(result->clipboard, NULL, 0);

	result->file_edited = FALSE;

	result->data = data;
	result->data->main_view = HILDON_WINDOW(hildon_window_new()); 

	create_treeview(result);
	create_textarea(result);
	create_checklist(result);
	
	result->brushsizemenu = create_brushsizemenu(result);
	result->sketchlinesmenu = create_sketchlinesmenu(result);
	result->viewmenu = create_viewmenu(result);
	result->shapemenu = create_shapemenu(result);
	
	create_toolbar(result);
	GtkWidget *nodemenu = create_menu(result);

	g_signal_connect_swapped(result->treeview, "button_press_event", G_CALLBACK(cb_popup), nodemenu);
	gtk_widget_tap_and_hold_setup(GTK_WIDGET(result->treeview), nodemenu, NULL, GTK_TAP_AND_HOLD_NONE);

	gtk_container_add(GTK_CONTAINER(result->data->main_view), result->hpaned);

	gtk_paned_add1(GTK_PANED(result->hpaned), result->scrolledtree);

	result->sk = sketchwidget_new(800, 480, FALSE);
	sketchwidget_set_undocallback(result->sk, callback_undotoggle, result);
	sketchwidget_set_redocallback(result->sk, callback_redotoggle, result);
/*	sketchwidget_set_fingercallback(result->sk, callback_finger, result);*/

	gtk_box_pack_start(GTK_BOX(main_vbox), result->scrolledwindow, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(main_vbox), sketchwidget_get_mainwidget(result->sk), TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(main_vbox), result->listscroll, TRUE, TRUE, 0);
	hildon_window_add_toolbar(result->data->main_view, GTK_TOOLBAR(result->toolbar)); 

	gtk_paned_add2(GTK_PANED(result->hpaned), main_vbox);

	gtk_widget_show(main_vbox);
	gtk_widget_show(result->hpaned);

	gtk_widget_show_all(GTK_WIDGET(result->data->main_view));
	gtk_widget_grab_focus(GTK_WIDGET(result->textview));

	result->file_edited = FALSE;
	result->file_name = NULL;

	g_signal_connect(G_OBJECT(result->data->main_view), "key_press_event", G_CALLBACK(key_press_cb), result);
	g_signal_connect(G_OBJECT(result->data->main_view), "delete_event", G_CALLBACK(close_cb), result);

	sk_set_brushsize(result, 2);
	sk_set_brushsize(result, 4);

	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(result->sketchlinesmenuitems[0]), TRUE);
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(result->shapemenuitems[0]), TRUE);

	result->newnodedialog_createchild = TRUE;

	result->cursorBusy = gdk_cursor_new(GDK_WATCH);
	result->loading=FALSE;

	return result;
}

void interface_main_view_destroy(MainView * main)
{
	gdk_cursor_unref(main->cursorBusy);
	g_free(main);
}

gchar *interface_file_chooser(MainView * mainview, GtkFileChooserAction action, gchar * suggname, gchar * suggext)
{
	GtkWidget *dialog;
	gchar *filename = NULL;

	dialog = hildon_file_chooser_dialog_new(GTK_WINDOW(mainview->data->main_view), action);
	gtk_file_chooser_set_show_hidden(GTK_FILE_CHOOSER(dialog), TRUE);

	if (action == GTK_FILE_CHOOSER_ACTION_SAVE && suggname != NULL)
		{
			gchar fn[128];

			if (suggext == NULL)
				snprintf(fn, sizeof(fn), "%s", suggname);
			else
				snprintf(fn, sizeof(fn), "%s.%s", suggname, suggext);

			int i = 0;
			struct stat s;

			while(stat(fn, &s) == 0)
				{
					i++;
					if (suggext == NULL)
						snprintf(fn, sizeof(fn), "%s(%d)", suggname, i);
					else
						snprintf(fn, sizeof(fn), "%s(%d).%s", suggname, i, suggext);
				}

			fprintf(stderr, "%s\n", fn);
			gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(dialog), fn);
		}

	gtk_widget_show_all(GTK_WIDGET(dialog));
	if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
		{
			filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(dialog));
		}

	gtk_widget_destroy(dialog);
	return filename;
}

gint interface_create_new_file(MainView * main)
{
	HildonNote *hn = NULL;
	gint response = FALSE;

	g_assert(main != NULL && main->data->app != NULL);

	hn = HILDON_NOTE(hildon_note_new_confirmation_add_buttons(GTK_WINDOW(main->data->main_view), _("Create new file?"), _("Yes"), CONFRESP_YES, _("No"), CONFRESP_NO, NULL, NULL));
	response = gtk_dialog_run(GTK_DIALOG(hn));
	gtk_widget_destroy(GTK_WIDGET(hn));
	return response;
}

/*
 * Privates 
 */

/*
 * Create the menu items needed for the drop down menu 
 */
GtkWidget *create_menu(MainView * main)
{
	/*
	 * Create needed handles 
	 */
	GtkMenu *main_menu;
	GtkWidget *file_menu, *close, *about, *edit_menu, *tools_menu, *node_menu, *node_item,*move_node_menu;

	/*
	 * Get the menu of our view 
	 */
	main_menu = GTK_MENU( gtk_menu_new () );
	/* Add menu to HildonWindow */
	hildon_window_set_menu(main->data->main_view, main_menu); 

	/*
	 * Create new menus for submenus in our drop down menu 
	 */
	file_menu = gtk_menu_new();
	node_menu = gtk_menu_new();
	edit_menu = gtk_menu_new();
	tools_menu = gtk_menu_new();
	move_node_menu = gtk_menu_new();

	/*
	 * Create the menu items 
	 */
	main->file_item = gtk_menu_item_new_with_label(_("File"));
	node_item = gtk_menu_item_new_with_label(_("Memo"));

	main->new_node_item = gtk_menu_item_new_with_label(_("New memo"));
	main->delete_node_item = gtk_menu_item_new_with_label(_("Delete memo"));
	main->rename_node_item = gtk_menu_item_new_with_label(_("Rename memo"));
	main->expand_collapse_node_item = gtk_menu_item_new_with_label(_("Expand/collapse node"));
	main->export_node_item = gtk_menu_item_new_with_label(_("Export memo"));

	main->move_node_item = gtk_menu_item_new_with_label(_("Move node"));
	main->move_up_node_item = gtk_menu_item_new_with_label(_("Move up"));
	main->move_down_node_item = gtk_menu_item_new_with_label(_("Move down"));
	main->move_to_bottom_node_item = gtk_menu_item_new_with_label(_("Move to bottom"));
	main->move_to_top_node_item = gtk_menu_item_new_with_label(_("Move to top"));

	main->new_item = gtk_menu_item_new_with_label(_("New data file"));
	main->open_item = gtk_menu_item_new_with_label(_("Open data file..."));
	main->save_item = gtk_menu_item_new_with_label(_("Save data"));
	main->view_item = gtk_menu_item_new_with_label(_("View"));
	main->edit_item = gtk_menu_item_new_with_label(_("Edit"));
	main->cut_item = gtk_menu_item_new_with_label(_("Cut"));
	main->copy_item = gtk_menu_item_new_with_label(_("Copy"));
	main->paste_item = gtk_menu_item_new_with_label(_("Paste"));
	main->clear_item = gtk_menu_item_new_with_label(_("Clear"));
	main->tools_item = gtk_menu_item_new_with_label(_("Tools"));
	close = gtk_menu_item_new_with_label(_("Exit"));

	/*
	 * Add menu items to right menus 
	 */
	gtk_menu_append(main_menu, main->file_item);
	gtk_menu_append(file_menu, main->new_item);
	gtk_menu_append(file_menu, main->open_item);
	gtk_menu_append(file_menu, main->save_item);

	gtk_menu_append(main_menu, main->view_item);

	gtk_menu_append(main_menu, main->edit_item);
	gtk_menu_append(edit_menu, main->cut_item);
	gtk_menu_append(edit_menu, main->copy_item);
	gtk_menu_append(edit_menu, main->paste_item);
	gtk_menu_append(edit_menu, gtk_separator_menu_item_new());
	gtk_menu_append(edit_menu, main->clear_item);

	gtk_menu_append(main_menu, node_item);
	gtk_menu_append(node_menu, main->new_node_item);
	gtk_menu_append(node_menu, main->delete_node_item);
	gtk_menu_append(node_menu, main->rename_node_item);
	gtk_menu_append(node_menu, main->move_node_item);
	gtk_menu_append(node_menu, gtk_separator_menu_item_new());
	gtk_menu_append(node_menu, main->export_node_item);
#ifdef EXPANDING_ROWS
	gtk_menu_append(node_menu, gtk_separator_menu_item_new());
	gtk_menu_append(node_menu, main->expand_collapse_node_item);
#endif

	/*move node sub-menu*/
	gtk_menu_append(move_node_menu, main->move_up_node_item);
	gtk_menu_append(move_node_menu, main->move_down_node_item);
	gtk_menu_append(move_node_menu, main->move_to_bottom_node_item);
	gtk_menu_append(move_node_menu, main->move_to_top_node_item);

	gtk_menu_append(main_menu, main->tools_item);
	gtk_menu_append(main_menu, gtk_separator_menu_item_new());
	about = gtk_image_menu_item_new_from_stock(GTK_STOCK_ABOUT, NULL);
	gtk_menu_append(main_menu, about);
	gtk_menu_append(main_menu, gtk_separator_menu_item_new());
	gtk_menu_append(main_menu, close);

	g_signal_connect(G_OBJECT(about), "activate", G_CALLBACK(callback_about), main);

	/*
	 * Add submenus to the right items 
	 */
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->file_item), file_menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(node_item), node_menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->edit_item), edit_menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_item), tools_menu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->move_node_item), move_node_menu);

	main->tools_brushsize = gtk_menu_item_new_with_label(_("Brush size"));
	main->tools_pagestyle = gtk_menu_item_new_with_label(_("Page style"));
	main->tools_shape = gtk_menu_item_new_with_label(_("Shape"));
	main->tools_color = gtk_menu_item_new_with_label(_("Color..."));
	main->tools_font = gtk_menu_item_new_with_label(_("Font..."));
	main->tools_wordwrap = gtk_check_menu_item_new_with_label(_("Word wrap"));
	main->tools_pressure = gtk_check_menu_item_new_with_label(_("Pressure sensitive brush color"));

	gtk_menu_append(tools_menu, main->tools_color);
	gtk_menu_append(tools_menu, main->tools_brushsize);
	gtk_menu_append(tools_menu, main->tools_shape);
	gtk_menu_append(tools_menu, main->tools_pagestyle);
	gtk_menu_append(tools_menu, main->tools_font);
	gtk_menu_append(tools_menu, main->tools_wordwrap);
	gtk_menu_append(tools_menu, main->tools_pressure);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_brushsize), main->brushsizemenu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_pagestyle), main->sketchlinesmenu);
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->tools_shape), main->shapemenu);

	gtk_menu_item_set_submenu(GTK_MENU_ITEM(main->view_item), main->viewmenu);

	/*
	 * Attach the callback functions to the activate signal 
	 */
	g_signal_connect(G_OBJECT(main->new_node_item), "activate", G_CALLBACK(callback_file_new_node), main);
	g_signal_connect(G_OBJECT(main->delete_node_item), "activate", G_CALLBACK(callback_file_delete_node), main);
	g_signal_connect(G_OBJECT(main->rename_node_item), "activate", G_CALLBACK(callback_file_rename_node), main);
	g_signal_connect(G_OBJECT(main->export_node_item), "activate", G_CALLBACK(callback_file_export_node), main);
	g_signal_connect(G_OBJECT(main->expand_collapse_node_item), "activate", G_CALLBACK(callback_file_expand_collapse_node), main);
	
	g_signal_connect(G_OBJECT(main->move_up_node_item), "activate", G_CALLBACK(callback_move_up_node), main);
	g_signal_connect(G_OBJECT(main->move_down_node_item), "activate", G_CALLBACK(callback_move_down_node), main);
	g_signal_connect(G_OBJECT(main->move_to_top_node_item), "activate", G_CALLBACK(callback_move_to_top_level_node), main);
	g_signal_connect(G_OBJECT(main->move_to_bottom_node_item), "activate", G_CALLBACK(callback_move_to_bottom_level_node), main);
	
	g_signal_connect(G_OBJECT(main->new_item), "activate", G_CALLBACK(callback_file_new), main);
	g_signal_connect(G_OBJECT(main->open_item), "activate", G_CALLBACK(callback_file_open), main);
	g_signal_connect(G_OBJECT(main->save_item), "activate", G_CALLBACK(callback_file_save), main);
	g_signal_connect(G_OBJECT(main->cut_item), "activate", G_CALLBACK(callback_edit_cut), main);
	g_signal_connect(G_OBJECT(main->copy_item), "activate", G_CALLBACK(callback_edit_copy), main);
	g_signal_connect(G_OBJECT(main->paste_item), "activate", G_CALLBACK(callback_edit_paste), main);
	g_signal_connect(G_OBJECT(main->clear_item), "activate", G_CALLBACK(callback_edit_clear), main);
	g_signal_connect(G_OBJECT(main->tools_font), "activate", G_CALLBACK(callback_font), main);
	g_signal_connect(G_OBJECT(main->tools_wordwrap), "toggled", G_CALLBACK(callback_wordwrap), main);
	g_signal_connect(G_OBJECT(main->tools_pressure), "toggled", G_CALLBACK(callback_pressure), main);
	g_signal_connect(G_OBJECT(main->tools_color), "activate", G_CALLBACK(callback_color_invoke), main);

	g_signal_connect(G_OBJECT(close), "activate", G_CALLBACK(callback_file_close), main);

	/*
	 * We need to show menu items 
	 */
	gtk_widget_show_all(GTK_WIDGET(main_menu));

	return (node_menu);
}

GtkWidget *create_brushsizemenu(MainView * main)
{
	GtkWidget *menu;
	int i;

	GSList *gr = NULL;

	menu = gtk_menu_new();

	for(i = 0; i < BRUSHSIZE_COUNT; i++)
		{
			int bsize = (i + 1) * 2;

			char fpa[PATH_MAX];

			snprintf(fpa, sizeof(fpa), "%s/pencil%d.png", PIXMAPDIR, i + 1);
			GtkWidget *pix = gtk_image_new_from_file(fpa);

			g_object_ref(pix);

			char st[128];

			snprintf(st, sizeof(st), _("%dpt"), bsize);
			main->brushsizemenuitems[i] = gtk_radio_menu_item_new_with_label(gr, st);
			gr = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(main->brushsizemenuitems[i]));

			gtk_object_set_user_data(GTK_OBJECT(main->brushsizemenuitems[i]), bsize);
			gtk_object_set_data(GTK_OBJECT(main->brushsizemenuitems[i]), "m", main);
			gtk_object_set_data(GTK_OBJECT(main->brushsizemenuitems[i]), "i", pix);

			gtk_menu_prepend(menu, main->brushsizemenuitems[i]);
			g_signal_connect(G_OBJECT(main->brushsizemenuitems[i]), "activate", G_CALLBACK(callback_brushsize), main->brushsizemenuitems[i]);
		}

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}

GtkWidget *create_sketchlinesmenu(MainView * main)
{
	GtkWidget *menu;
	int i;

	GSList *gr = NULL;

	menu = gtk_menu_new();

	char st[3][128];

	snprintf(st[0], sizeof(st[0]), _("No background"));
	snprintf(st[1], sizeof(st[1]), _("Notebook lines"));
	snprintf(st[2], sizeof(st[2]), _("Graph lines"));

	for(i = 0; i < 3; i++)
		{
			char fpa[PATH_MAX];

			snprintf(fpa, sizeof(fpa), "%s/sketchbg%d.png", PIXMAPDIR, i);
			GtkWidget *pix = gtk_image_new_from_file(fpa);

			g_object_ref(pix);

			main->sketchlinesmenuitems[i] = gtk_radio_menu_item_new_with_label(gr, st[i]);
			gr = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(main->sketchlinesmenuitems[i]));

			gtk_object_set_user_data(GTK_OBJECT(main->sketchlinesmenuitems[i]), i);
			gtk_object_set_data(GTK_OBJECT(main->sketchlinesmenuitems[i]), "m", main);
			gtk_object_set_data(GTK_OBJECT(main->sketchlinesmenuitems[i]), "i", pix);

			gtk_menu_prepend(menu, main->sketchlinesmenuitems[i]);
			g_signal_connect(G_OBJECT(main->sketchlinesmenuitems[i]), "activate", G_CALLBACK(callback_sketchlines), main->sketchlinesmenuitems[i]);
		}

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}

GtkWidget *create_viewmenu(MainView * main)
{
	GtkWidget *menu;
	int i;

	menu = gtk_menu_new();

	char st[3][128];
	int stf[3];

	snprintf(st[0], sizeof(st[0]), _("Tree"));
	snprintf(st[1], sizeof(st[1]), _("Toolbar"));
	snprintf(st[2], sizeof(st[2]), _("Full screen"));

/*
this is here because we might want to change the order of the items
warning: these are also in callback_setview()
*/

	stf[0]=1;
	stf[1]=2;
	stf[2]=4;

	for(i = 0; i < 3; i++)
		{
			main->viewmenuitems[i] = gtk_check_menu_item_new_with_label(st[i]);

			gtk_object_set_user_data(GTK_OBJECT(main->viewmenuitems[i]), stf[i]);
			gtk_object_set_data(GTK_OBJECT(main->viewmenuitems[i]), "m", main);

			gtk_menu_prepend(menu, main->viewmenuitems[i]);
			g_signal_connect(G_OBJECT(main->viewmenuitems[i]), "toggled", G_CALLBACK(callback_viewmenu), main->viewmenuitems[i]);
		}

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}

GtkWidget *create_shapemenu(MainView * main)
{
	GtkWidget *menu;
	int i;

	GSList *gr = NULL;

	menu = gtk_menu_new();

	char st[4][128];

	snprintf(st[0], sizeof(st[0]), _("Free"));
	snprintf(st[1], sizeof(st[1]), _("Line"));
	snprintf(st[2], sizeof(st[2]), _("Rectangle"));
	snprintf(st[3], sizeof(st[3]), _("Ellipse"));

	for(i = 0; i < 4; i++)
		{
			main->shapemenuitems[i] = gtk_radio_menu_item_new_with_label(gr, st[i]);
			gr = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(main->shapemenuitems[i]));

			gtk_object_set_user_data(GTK_OBJECT(main->shapemenuitems[i]), i);
			gtk_object_set_data(GTK_OBJECT(main->shapemenuitems[i]), "m", main);

			gtk_menu_prepend(menu, main->shapemenuitems[i]);
			g_signal_connect(G_OBJECT(main->shapemenuitems[i]), "activate", G_CALLBACK(callback_shapemenu), main->shapemenuitems[i]);
		}

	gtk_widget_show_all(GTK_WIDGET(menu));

	return (menu);
}
/*
 * Create toolbar to mainview 
 */
static void create_toolbar(MainView * main)
{
        GtkToolItem * ti;
        
	/*
	 * Create new GTK toolbar 
	 */
	main->toolbar = gtk_toolbar_new();

	/*
	 * Set toolbar properties 
	 */
	gtk_toolbar_set_orientation(GTK_TOOLBAR(main->toolbar), GTK_ORIENTATION_HORIZONTAL);
	gtk_toolbar_set_style(GTK_TOOLBAR(main->toolbar), GTK_TOOLBAR_BOTH_HORIZ);

	/*
	 * Make menus and buttons to toolbar: 
	 */

/*	main->toggletree_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_INDEX);*/
	main->toggletree_tb = gtk_toggle_tool_button_new();
/* FIXME: dirty */
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->toggletree_tb), "qgn_toolb_rss_fldonoff");
	g_signal_connect(G_OBJECT(main->toggletree_tb), "toggled", G_CALLBACK(callback_toggletree), main);

	main->new_tb = gtk_tool_button_new_from_stock(GTK_STOCK_ADD);

/*	main->save_tb = gtk_tool_button_new_from_stock(GTK_STOCK_SAVE);*/
	main->save_tb = gtk_tool_button_new(NULL, NULL);
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->save_tb), "qgn_toolb_gene_save");

	main->separator_tb1 = gtk_separator_tool_item_new();

	/*
	 * main->cut_tb = gtk_tool_button_new_from_stock(GTK_STOCK_CUT);
	 * main->copy_tb = gtk_tool_button_new_from_stock(GTK_STOCK_COPY);
	 * main->paste_tb = gtk_tool_button_new_from_stock(GTK_STOCK_PASTE);
	 * main->separator_tb2 = gtk_separator_tool_item_new();
	 */

	main->bold_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->bold_tb), "qgn_list_gene_bold");

	main->italic_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->italic_tb), "qgn_list_gene_italic");

	main->underline_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->underline_tb), "qgn_list_gene_underline");

	main->bullet_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->bullet_tb), "qgn_list_gene_bullets");

	main->strikethru_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_STRIKETHROUGH);
/*	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->strikethru_tb), "qgn_list_gene_strikethrough");*/

	main->check_tb = gtk_toggle_tool_button_new();
	_tool_button_set_icon_name(GTK_TOOL_BUTTON(main->check_tb), "qgn_list_continuous_multi_select");

	main->checkadd_tb = gtk_tool_button_new_from_stock(GTK_STOCK_FILE);
	main->checkdel_tb = gtk_tool_button_new_from_stock(GTK_STOCK_DELETE);

/*		       	
	main->bold_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_BOLD);
	main->italic_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_ITALIC);
	main->underline_tb = gtk_toggle_tool_button_new_from_stock(GTK_STOCK_UNDERLINE);
*/
	main->font_tb = gtk_tool_button_new_from_stock(GTK_STOCK_SELECT_FONT);

/*	main->fullscreen_tb = gtk_tool_button_new_from_stock(GTK_STOCK_ZOOM_FIT);*/

	/*
	 * Insert items to toolbar 
	 */
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->toggletree_tb, -1);
/*	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), gtk_separator_tool_item_new(), -1);*/
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->new_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->save_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->separator_tb1, -1);

	/*
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->cut_tb, -1);
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->copy_tb, -1);
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->paste_tb, -1);
	 * gtk_toolbar_insert ( GTK_TOOLBAR(main->toolbar), main->separator_tb2, -1);
	 */
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->font_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->bold_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->italic_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->underline_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->bullet_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->strikethru_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->check_tb, -1);

	GdkColor col;

	col.red = 0;
	col.green = 0;
	col.blue = 0;
	main->colorbutton = hildon_color_button_new_with_color(&col);

	main->colorbutton_tb = gtk_tool_item_new();
	gtk_container_add(GTK_CONTAINER(main->colorbutton_tb), main->colorbutton);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->colorbutton_tb, -1);
	g_signal_connect(G_OBJECT(main->colorbutton), "clicked", G_CALLBACK(callback_color), main);

	main->brushsize_tb = gtk_tool_button_new(NULL, NULL);
	g_signal_connect(G_OBJECT(main->brushsize_tb), "clicked", G_CALLBACK(callback_brushsizetb), main);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->brushsize_tb, -1);

	main->eraser_tb = gtk_toggle_tool_button_new();
	gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(main->eraser_tb), gtk_image_new_from_file(PIXMAPDIR "/" "eraser.png"));
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->eraser_tb, -1);
	g_signal_connect(G_OBJECT(main->eraser_tb), "toggled", G_CALLBACK(callback_eraser), main);

	main->shape_tb = gtk_tool_button_new_from_stock(GTK_STOCK_CONVERT);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->shape_tb, -1);
	g_signal_connect(G_OBJECT(main->shape_tb), "clicked", G_CALLBACK(callback_menu), main->shapemenu);

	main->sketchlines_tb = gtk_tool_button_new(NULL, NULL);
	g_signal_connect(G_OBJECT(main->sketchlines_tb), "clicked", G_CALLBACK(callback_menu), main->sketchlinesmenu);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->sketchlines_tb, -1);

	
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), gtk_separator_tool_item_new(), -1);

	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->checkadd_tb, -1);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->checkdel_tb, -1);

	main->undo_tb = gtk_tool_button_new_from_stock(GTK_STOCK_UNDO);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->undo_tb, -1);
	g_signal_connect(G_OBJECT(main->undo_tb), "clicked", G_CALLBACK(callback_undo), main);

	main->redo_tb = gtk_tool_button_new_from_stock(GTK_STOCK_REDO);
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), main->redo_tb, -1);
	g_signal_connect(G_OBJECT(main->redo_tb), "clicked", G_CALLBACK(callback_redo), main);

        /* Search entry widget */
	gtk_toolbar_insert(GTK_TOOLBAR(main->toolbar), gtk_separator_tool_item_new(), -1);
        ti = gtk_tool_item_new();
        gtk_tool_item_set_expand(ti, TRUE);
        main->search_entry = gtk_entry_new();
        g_signal_connect(G_OBJECT(main->search_entry), "activate", G_CALLBACK(callback_search_activate), main);
        gtk_container_add(GTK_CONTAINER(ti), main->search_entry);
        gtk_toolbar_insert(main->toolbar, ti, -1);
        ti = gtk_tool_button_new(gtk_image_new_from_icon_name("qgn_addr_icon_search_service", GTK_ICON_SIZE_SMALL_TOOLBAR), _("Search"));
        g_signal_connect(G_OBJECT(ti), "clicked", G_CALLBACK(callback_search_activate), main);
        gtk_toolbar_insert(main->toolbar, ti, -1);

	/*
	 * Connect signals to buttons 
	 */
	g_signal_connect(G_OBJECT(main->new_tb), "clicked", G_CALLBACK(callback_file_new_node), main);
	g_signal_connect(G_OBJECT(main->save_tb), "clicked", G_CALLBACK(callback_file_save), main);

	/*
	 * g_signal_connect(G_OBJECT(main->cut_tb), "clicked",
	 * G_CALLBACK(callback_edit_cut), main);
	 * g_signal_connect(G_OBJECT(main->copy_tb), "clicked",
	 * G_CALLBACK(callback_edit_copy), main);
	 * g_signal_connect(G_OBJECT(main->paste_tb), "clicked",
	 * G_CALLBACK(callback_edit_paste), main);
	 */
	g_signal_connect(G_OBJECT(main->font_tb), "clicked", G_CALLBACK(callback_font), main);

	gtk_object_set_data(GTK_OBJECT(main->bold_tb), "s", WPT_BOLD);
	gtk_object_set_data(GTK_OBJECT(main->italic_tb), "s", WPT_ITALIC);
	gtk_object_set_data(GTK_OBJECT(main->underline_tb), "s", WPT_UNDERLINE);
	gtk_object_set_data(GTK_OBJECT(main->bullet_tb), "s", WPT_BULLET);
	gtk_object_set_data(GTK_OBJECT(main->strikethru_tb), "s", WPT_STRIKE);
	gtk_object_set_data(GTK_OBJECT(main->check_tb), "s", WPT_LEFT);

	gtk_object_set_data(GTK_OBJECT(main->bold_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->italic_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->underline_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->bullet_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->strikethru_tb), "m", main);
	gtk_object_set_data(GTK_OBJECT(main->check_tb), "m", main);

	g_signal_connect(G_OBJECT(main->bold_tb), "toggled", G_CALLBACK(callback_fontstyle), main->bold_tb);
	g_signal_connect(G_OBJECT(main->italic_tb), "toggled", G_CALLBACK(callback_fontstyle), main->italic_tb);
	g_signal_connect(G_OBJECT(main->underline_tb), "toggled", G_CALLBACK(callback_fontstyle), main->underline_tb);
	g_signal_connect(G_OBJECT(main->bullet_tb), "toggled", G_CALLBACK(callback_fontstyle), main->bullet_tb);
	g_signal_connect(G_OBJECT(main->strikethru_tb), "toggled", G_CALLBACK(callback_fontstyle), main->strikethru_tb);
	g_signal_connect(G_OBJECT(main->check_tb), "toggled", G_CALLBACK(callback_fontstyle), main->check_tb);

	g_signal_connect(G_OBJECT(main->checkadd_tb), "clicked", G_CALLBACK(callback_checklist_add), main);
	g_signal_connect(G_OBJECT(main->checkdel_tb), "clicked", G_CALLBACK(callback_checklist_delete), main);

	gtk_widget_show_all(GTK_WIDGET(main->toolbar));
}

/*
 * Create the text area 
 */
void create_textarea(MainView * main)
{
	main->scrolledwindow = gtk_scrolled_window_new(NULL, NULL);
#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        hildon_helper_set_thumb_scrollbar(main->scrolledwindow, TRUE);
#endif
	gtk_widget_hide(main->scrolledwindow);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(main->scrolledwindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	main->textview = wp_text_view_new();
/*
	gtk_text_view_set_editable(GTK_TEXT_VIEW(main->textview), TRUE);
	gtk_text_view_set_left_margin(GTK_TEXT_VIEW(main->textview), 10);
	gtk_text_view_set_right_margin(GTK_TEXT_VIEW(main->textview), 10);
	main->buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(main->textview));
	main->buffer = wp_text_buffer_new(NULL);
*/
	main->buffer = WP_TEXT_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(main->textview)));
	
	g_signal_connect(G_OBJECT(main->buffer), "refresh_attributes", G_CALLBACK(callback_textbuffer_move), main);
	g_signal_connect(G_OBJECT(main->buffer), "can_undo", G_CALLBACK(callback_undotoggle), main);
	g_signal_connect(G_OBJECT(main->buffer), "can_redo", G_CALLBACK(callback_redotoggle), main);

	wp_text_buffer_enable_rich_text(main->buffer, TRUE);
	gtk_text_buffer_set_can_paste_rich_text(GTK_TEXT_BUFFER(main->buffer), TRUE);
	
	gtk_container_add(GTK_CONTAINER(main->scrolledwindow), main->textview);
	gtk_widget_show(main->textview);

	gtk_widget_modify_font(main->textview, pango_font_description_from_string("Monospace Regular 16"));

	g_signal_connect(G_OBJECT(main->buffer), "modified-changed", G_CALLBACK(callback_buffer_modified), main);
	g_signal_connect(G_OBJECT(main->buffer), "changed", G_CALLBACK(callback_buffer_modified), main);
}

/*
 * Create the treeview 
 */
void create_treeview(MainView * main)
{
        main->color_highlight = "#ffff00";
	main->scrolledtree = gtk_scrolled_window_new(NULL, NULL);
/*#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        hildon_helper_set_thumb_scrollbar(main->scrolledtree, TRUE);
#endif*/
	gtk_widget_show(main->scrolledtree);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(main->scrolledtree), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	GtkTreeStore *tstore = gtk_tree_store_new(N_COLUMNS, G_TYPE_STRING, GDK_TYPE_PIXBUF, G_TYPE_POINTER, G_TYPE_STRING);

	main->treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(tstore));
	g_object_unref(G_OBJECT(tstore));

	GtkCellRenderer *renderer1 = gtk_cell_renderer_text_new();
	GtkCellRenderer *renderer2 = gtk_cell_renderer_pixbuf_new();

	GtkTreeViewColumn *column = gtk_tree_view_column_new();

	gtk_tree_view_column_pack_start(column, renderer2, FALSE);
	gtk_tree_view_column_pack_start(column, renderer1, FALSE);

	gtk_tree_view_column_add_attribute(column, renderer1, "cell-background", NODE_BGCOLOR);
	gtk_tree_view_column_add_attribute(column, renderer1, "text", NODE_NAME);
	gtk_tree_view_column_add_attribute(column, renderer2, "pixbuf", NODE_PIXBUF);

	/*
	 * gtk_tree_view_column_add_attribute(column, renderer2, "width", 0);
	 * gtk_tree_view_column_add_attribute(column, renderer2, "height", 0);
	 */
	gtk_tree_view_column_set_cell_data_func(column, renderer2, callback_treeview_celldatafunc, main, NULL);

	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_FIXED);
	gtk_tree_view_column_set_resizable(column, FALSE);
	
	gtk_tree_view_append_column(GTK_TREE_VIEW(main->treeview), column);

	GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(main->treeview));

	gtk_tree_selection_set_mode(select, GTK_SELECTION_BROWSE);
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(main->treeview), FALSE);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(main->treeview), FALSE);
#ifdef EXPANDING_ROWS
	gtk_tree_view_set_expander_column(GTK_TREE_VIEW(main->treeview), column);
#else
	gtk_tree_view_set_expander_column(GTK_TREE_VIEW(main->treeview), NULL);
#endif

	gtk_tree_view_expand_all(GTK_TREE_VIEW(main->treeview));
	gtk_tree_view_set_enable_search(GTK_TREE_VIEW(main->treeview), TRUE);
	gtk_tree_view_set_fixed_height_mode(GTK_TREE_VIEW(main->treeview), FALSE);
	gtk_tree_view_set_enable_tree_lines(GTK_TREE_VIEW(main->treeview), TRUE);

	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(callback_treeview_change), main);
#ifndef EXPANDING_ROWS
	g_signal_connect(G_OBJECT(main->treeview), "test-collapse-row", G_CALLBACK(callback_treeview_testcollapse), main);
#endif
	gtk_tree_selection_set_select_function(select, treeview_canselect, main, NULL);
	gtk_object_set_user_data(GTK_OBJECT(main->treeview), (gpointer) main);

	gtk_container_add(GTK_CONTAINER(main->scrolledtree), main->treeview);

	gtk_widget_set_name(main->treeview, "maemopadplus-treeview");
	
	gtk_rc_parse(PIXMAPDIR "/maemopadplus.rc");

	gtk_widget_show(main->treeview);
}


void create_checklist(MainView * main)
{
	main->listscroll = gtk_scrolled_window_new(NULL, NULL);
#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        hildon_helper_set_thumb_scrollbar(main->listscroll, TRUE);
#endif
	gtk_widget_hide(main->listscroll);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(main->listscroll), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	GtkListStore *store = gtk_list_store_new(CHECKN_COLUMNS, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_INT, G_TYPE_BOOLEAN);

	main->listview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
	g_object_unref(G_OBJECT(store));

	g_object_set(main->listview, "allow-checkbox-mode", FALSE, NULL);

	GtkCellRenderer *renderer1 = gtk_cell_renderer_toggle_new();
	g_object_set(renderer1, "activatable", TRUE, NULL);
	g_object_set(renderer1, "checkbox-mode", FALSE, NULL);
	g_object_set(renderer1, "radio", FALSE, NULL);
	g_signal_connect(G_OBJECT(renderer1), "toggled", G_CALLBACK(callback_checklist_toggled), main->listview);

	GtkCellRenderer *renderer2 = gtk_cell_renderer_text_new();
	g_object_set(renderer2, "editable", TRUE, NULL);
#ifdef MAEMOPADPLUS_FINGER_FRIENDLY
        g_object_set(renderer2, "font", MAEMOPADPLUS_FINGER_FONT, NULL);
#endif
	g_signal_connect(G_OBJECT(renderer2), "edited", G_CALLBACK(callback_checklist_edited), main->listview);
 
	GtkTreeViewColumn *col1 = gtk_tree_view_column_new_with_attributes("", renderer1, 
/*                                                      "cell-background", CHECKNODE_COLOR,*/
                                                      "active", CHECKNODE_CHECKED,
                                                      NULL);

	main->listtextcol = gtk_tree_view_column_new_with_attributes("", renderer2, 
                                                      "text", CHECKNODE_TEXT,
/*                                                      "cell-background", CHECKNODE_COLOR,*/
                                                      "foreground", CHECKNODE_COLOR,
                                                      "strikethrough", CHECKNODE_STRIKE,
                                                      "weight", CHECKNODE_BOLD,
                                                      NULL);

	gtk_tree_view_append_column(GTK_TREE_VIEW(main->listview), col1);
	gtk_tree_view_append_column(GTK_TREE_VIEW(main->listview), main->listtextcol);

	GtkTreeSelection *select = gtk_tree_view_get_selection(GTK_TREE_VIEW(main->listview));

	gtk_tree_selection_set_mode(select, GTK_SELECTION_MULTIPLE);
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(main->listview), FALSE);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(main->listview), FALSE);
	gtk_tree_view_set_enable_search(GTK_TREE_VIEW(main->listview), TRUE);

/*
	gtk_tree_selection_set_select_function(select, treeview_canselect, main, NULL);
*/
	g_signal_connect(G_OBJECT(select), "changed", G_CALLBACK(callback_checklist_change), main);

	gtk_object_set_user_data(GTK_OBJECT(main->listview), (gpointer) main);
	gtk_object_set_data(GTK_OBJECT(main->listview), "edited", FALSE);

	gtk_container_add(GTK_CONTAINER(main->listscroll), main->listview);
	gtk_widget_show(main->listview);
/*
GtkTreeIter    toplevel;
gtk_list_store_append(store, &toplevel);
gtk_list_store_set(store, &toplevel,
                     CHECKNODE_CHECKED, TRUE, CHECKNODE_TEXT, "sample text", -1);
*/	
}
