/*
 * This file is part of maemopad+
 *
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <ui/callbacks.h>
#include <ui/interface.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <libintl.h>

/*
 * strlen needed from string.h 
 */
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <config.h>
#include <time.h>

#include <hildon/hildon-banner.h>
#include <hildon/hildon-note.h>
#include <hildon/hildon-font-selection-dialog.h>
#include <tablet-browser-interface.h>

#include <libgnomevfs/gnome-vfs.h>


/*
 * Privates: 
 */
gboolean read_file_to_buffer(MainView * mainview);
void write_buffer_to_file(MainView * mainview);
void new_node_dialog(nodeType typ, MainView * mainview);
gboolean foreach_func_update_ord (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter, MainView *mainview);
void move_nodes_up(GtkTreeModel * model, GtkTreeRowReference * topnode, GtkTreeRowReference * newtop);
GtkTreeRowReference *iter2ref(GtkTreeModel * model, GtkTreeIter * iter);
gboolean exec_command_on_db(MainView *mainview,char sql_string[]);
int get_node_id_on_tmp_db(GtkTreeModel *model,GtkTreeIter *iter);
gint get_branch_node_index(GtkTreePath *path);
gboolean move_node(MainView *mainview,GtkTreeIter *new_parent,GtkTreeIter *item_to_move,GtkTreeIter *item_befor);
gboolean tree_model_iter_prev(GtkTreeModel *tree_model,GtkTreeIter *iter);

/*
st==0: turn off/zero refcount
st==1: turn on/increase ref
st==2: decrease refcount and turn off if necessary
*/
void setBusy(MainView *mainview, gchar st)
{
if (st==0) mainview->busyrefcount=0;
else if (st==1) mainview->busyrefcount++;
else if (st==2 && mainview->busyrefcount>0) mainview->busyrefcount--;
gdk_window_set_cursor(GTK_WIDGET(mainview->data->main_view)->window, (mainview->busyrefcount>0)?mainview->cursorBusy:NULL);
}

gboolean isBusy(MainView *mainview)
{
if (mainview->busyrefcount>0) return(TRUE);
return(FALSE);	
}

void prepareUIforNodeChange(MainView * mainview, nodeType typ)
{
	gtk_widget_set_sensitive(GTK_WIDGET(mainview->undo_tb), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(mainview->redo_tb), TRUE);

	if (typ == NODE_TEXT)
		{
			gtk_widget_show(GTK_WIDGET(mainview->font_tb));
			gtk_widget_show(GTK_WIDGET(mainview->bold_tb));
			gtk_widget_show(GTK_WIDGET(mainview->italic_tb));
			gtk_widget_show(GTK_WIDGET(mainview->underline_tb));
			gtk_widget_show(GTK_WIDGET(mainview->bullet_tb));
			gtk_widget_show(mainview->tools_font);
			gtk_widget_show(mainview->tools_wordwrap);
		}
	else
		{
			gtk_widget_hide(GTK_WIDGET(mainview->font_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->bold_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->italic_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->underline_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->bullet_tb));
			gtk_widget_hide(mainview->tools_font);
			gtk_widget_hide(mainview->tools_wordwrap);
		}

	if (typ == NODE_SKETCH)
		{
/*			gtk_widget_show(GTK_WIDGET(mainview->colorbutton_tb));*/
			gtk_widget_show(GTK_WIDGET(mainview->eraser_tb));
			gtk_widget_show(GTK_WIDGET(mainview->brushsize_tb));
			gtk_widget_show(GTK_WIDGET(mainview->sketchlines_tb));
			gtk_widget_show(GTK_WIDGET(mainview->shape_tb));
			gtk_widget_show(mainview->tools_color);
			gtk_widget_show(mainview->tools_brushsize);
			gtk_widget_show(mainview->tools_pagestyle);
			gtk_widget_show(mainview->tools_shape);
			gtk_widget_show(mainview->tools_pressure);
		}
	else
		{
/*			gtk_widget_hide(GTK_WIDGET(mainview->colorbutton_tb));*/
			gtk_widget_hide(GTK_WIDGET(mainview->eraser_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->brushsize_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->sketchlines_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->shape_tb));
			gtk_widget_hide(mainview->tools_color);
			gtk_widget_hide(mainview->tools_brushsize);
			gtk_widget_hide(mainview->tools_pagestyle);
			gtk_widget_hide(mainview->tools_shape);
			gtk_widget_hide(mainview->tools_pressure);
		}

	if (typ == NODE_CHECKLIST)
		{
			gtk_widget_show(GTK_WIDGET(mainview->bold_tb));
			gtk_widget_show(GTK_WIDGET(mainview->strikethru_tb));
			gtk_widget_show(GTK_WIDGET(mainview->check_tb));
			gtk_widget_show(GTK_WIDGET(mainview->checkadd_tb));
			gtk_widget_show(GTK_WIDGET(mainview->checkdel_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->undo_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->redo_tb));
		}
	else
		{
			gtk_widget_hide(GTK_WIDGET(mainview->strikethru_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->check_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->checkadd_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->checkdel_tb));
			gtk_widget_show(GTK_WIDGET(mainview->undo_tb));
			gtk_widget_show(GTK_WIDGET(mainview->redo_tb));
		}

	if (typ == NODE_TEXT)
		{
			gtk_widget_hide(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_hide(mainview->listscroll);
			gtk_widget_show(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_show(mainview->tools_item);
		}
	else if (typ == NODE_SKETCH)
		{
			gtk_widget_show(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_hide(mainview->listscroll);
			gtk_widget_show(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_show(mainview->tools_item);
		}
	else if (typ == NODE_CHECKLIST)
		{
			gtk_widget_show(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_hide(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_hide(mainview->tools_item);
			gtk_widget_show(mainview->listscroll);
		}
	else
		{
			gtk_widget_hide(GTK_WIDGET(mainview->colorbutton_tb));
			gtk_widget_hide(GTK_WIDGET(mainview->scrolledwindow));
			gtk_widget_hide(sketchwidget_get_mainwidget(mainview->sk));
			gtk_widget_hide(mainview->tools_item);
			gtk_widget_hide(mainview->listscroll);
		}
}

nodeData *getSelectedNode(MainView * mainview)
{
	GtkTreeIter iter;
	GtkTreeModel *model;

	nodeData *nd;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (!gtk_tree_selection_get_selected(selection, &model, &iter))
		return (NULL);

	gtk_tree_model_get(model, &iter, NODE_DATA, &nd, -1);

	return (nd);
}

void saveCurrentData(MainView * mainview)
{
	nodeData *selnode = getSelectedNode(mainview);
	saveDataToNode(mainview, selnode);
}

void saveDataToNode(MainView * mainview, nodeData *selnode)
{
	if (selnode == NULL)
		return;

	if (
	(selnode->typ == NODE_SKETCH && sketchwidget_get_edited(mainview->sk) == FALSE) ||
	(selnode->typ == NODE_TEXT && wp_text_buffer_is_modified(mainview->buffer)==FALSE) ||
	(selnode->typ == NODE_CHECKLIST && gtk_object_get_data(GTK_OBJECT(mainview->listview), "edited")==FALSE)
	)
		{
			fprintf(stderr, "node not edited, not saving\n");
			return;
		}

	mainview->file_edited = TRUE;

	setBusy(mainview, 1);
	fprintf(stderr, "savecurrentdata!\n");

	gboolean goterr = TRUE;
	gchar *textdata = NULL;
	GdkPixbuf *pixbuf = NULL;
	gchar *sketchdata = NULL;
	gsize datalen = 0;
	char tq[512];

	if (selnode->typ == NODE_TEXT)
		{
#if 0
			GtkTextIter start, end;
			gtk_text_buffer_get_bounds(GTK_TEXT_BUFFER(mainview->buffer), &start, &end);
			textdata = gtk_text_buffer_serialize_rich_text(GTK_TEXT_BUFFER(mainview->buffer), &start, &end, &datalen);
#endif
			
			GString *gstr=g_string_sized_new(4096);
			wp_text_buffer_save_document(mainview->buffer, (WPDocumentSaveCallback)(wp_savecallback), gstr);

			datalen=gstr->len;
			textdata=g_string_free(gstr, FALSE);
/*fprintf(stderr, "*%s*\n", textdata);*/

/*			snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, bodyblob=NULL, body=?, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);*/
			snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, bodyblob=?, body=NULL, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);
		}
	else if (selnode->typ == NODE_SKETCH)
		{
			GError *err = NULL;
			GdkPixmap *skpix = sketchwidget_get_Pixmap(mainview->sk);
			GtkWidget *skdr = sketchwidget_get_drawingarea(mainview->sk);

			pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(skpix), NULL, 0, 0, 0, 0, skdr->allocation.width, skdr->allocation.height);
			if (pixbuf == NULL)
				{
					fprintf(stderr, "Error saving: pixbuf is null\n");
				}
			else
				{
				double w, h;
				GdkPixbuf *pixbuf2 = sketchwidget_trim_image(pixbuf, skdr->allocation.width, skdr->allocation.height, &w, &h, FALSE);

				if (pixbuf2!=NULL)
					{
					if (gdk_pixbuf_save_to_buffer(pixbuf2, &sketchdata, &datalen, "png", &err, NULL) == FALSE)
						{
							sketchdata = NULL;
							datalen = 0;
							fprintf(stderr, "Error saving sketch! %s\n", err->message);
							g_error_free(err);
						}
					gdk_pixbuf_unref(pixbuf2);
					}
				}
			snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, body=NULL, bodyblob=?, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);
			fprintf(stderr, "datalen:%d\n", datalen);
			if (skpix && G_IS_OBJECT(skpix)) g_object_unref(skpix);
		}
	else if (selnode->typ == NODE_CHECKLIST)
		{
		snprintf(tq, sizeof(tq), "UPDATE %s SET bodytype=%d, body=NULL, bodyblob=NULL, flags=%d WHERE nodeid=%d", datatable_tmpname, selnode->typ, selnode->flags, selnode->sql3id);
		}
	
/*fprintf(stderr, "query is *%s*\n", tq); */

	do
		{
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

			if (rc)
				{
					fprintf(stderr, "Error updating: %s\n", sqlite3_errmsg(mainview->db));
					break;
				}
			if (selnode->typ == NODE_TEXT)
				sqlite3_bind_text(stmt, 1, textdata, datalen, /*strlen(textdata),*/ SQLITE_TRANSIENT);
			else if (selnode->typ == NODE_SKETCH)
				sqlite3_bind_blob(stmt, 1, sketchdata, datalen, SQLITE_TRANSIENT);

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
				}
			sqlite3_finalize(stmt);

			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					fprintf(stderr, "Error saving node: %s\n", sqlite3_errmsg(mainview->db));
				}
			else
				{
					if (selnode->typ == NODE_TEXT)
						gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(mainview->buffer), FALSE);
					else if (selnode->typ == NODE_SKETCH)
						sketchwidget_set_edited(mainview->sk, FALSE);
					else if (selnode->typ == NODE_CHECKLIST)
						gtk_object_set_data(GTK_OBJECT(mainview->listview), "edited", FALSE);
					goterr = FALSE;
				}
		}while(FALSE);

	while(goterr==FALSE && selnode->typ == NODE_CHECKLIST)
		{
		GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));

		char tq[512];
		snprintf(tq, sizeof(tq), "DELETE FROM %s WHERE nodeid=%d", checklisttable_tmpname, selnode->sql3id);
		sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

		GtkTreeIter iter;
		if (gtk_tree_model_get_iter_first(model, &iter)==FALSE) break;
		
		do
			{
			gint styletoset_weight;
			gboolean styletoset_strike;
			gboolean ischecked;
			gchar *text;
			gchar *color;
			unsigned long col=0;

	  	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, CHECKNODE_BOLD, &styletoset_weight, CHECKNODE_STRIKE, &styletoset_strike, CHECKNODE_CHECKED, &ischecked, CHECKNODE_TEXT, &text, CHECKNODE_COLOR, &color, -1);

			GdkColor tmpcol;
			if (color!=NULL && strcmp(color, "(null)")!=0 && gdk_color_parse(color, &tmpcol))
				{
				col=((tmpcol.red>>8)<<16)|((tmpcol.green>>8)<<8)|(tmpcol.blue>>8);
/*				fprintf(stderr, "calculated color for node is *%lu*\n", col);*/
				}

			gint style=0;
			if (ischecked) style|=CHECKSTYLE_CHECKED;
			if (styletoset_weight==PANGO_WEIGHT_BOLD) style|=CHECKSTYLE_BOLD;
			if (styletoset_strike) style|=CHECKSTYLE_STRIKE;

			snprintf(tq, sizeof(tq), "INSERT INTO %s (nodeid, name, style, color, ord) VALUES(%d, ?, %d, %lu, 0)", checklisttable_tmpname, selnode->sql3id, style, col);
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

			if (rc)
				{
					goterr=TRUE;
					fprintf(stderr, "Error while saving checklist: %s\n", sqlite3_errmsg(mainview->db));
					break;
				}
			sqlite3_bind_text(stmt, 1, text, strlen(text), SQLITE_TRANSIENT);

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
				}
			sqlite3_finalize(stmt);

			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					goterr=TRUE;
					fprintf(stderr, "Error while saving checklist (2): %s\n", sqlite3_errmsg(mainview->db));
				}

			g_free(color);
			g_free(text);
						
			}while(gtk_tree_model_iter_next(model, &iter)==TRUE);
		
		break;
		}

	if (textdata)
		g_free(textdata);
	if (sketchdata)
		g_free(sketchdata);
	if (goterr == TRUE)
		{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error saving memo"));
		}
	setBusy(mainview, 2);
}

void callback_treeview_celldatafunc(GtkTreeViewColumn * tree_column, GtkCellRenderer * cell, GtkTreeModel * tree_model, GtkTreeIter * iter, gpointer data)
{
	MainView *mainview = ( MainView * ) data;
	g_assert(mainview != NULL && mainview->data != NULL );
	
	nodeData *nd;

	gtk_tree_model_get(tree_model, iter, NODE_DATA, &nd, -1);
	if (nd == NULL)
		return;

	if (nd->namepix == NULL)
		{
			g_object_set(cell, "visible", FALSE, NULL);
		}
	else
		{
			g_object_set(cell, "visible", TRUE, NULL);
			g_object_set(cell, "width", SKETCHNODE_RX, NULL);
			g_object_set(cell, "height", SKETCHNODE_RY, NULL);
		}
}

	
gboolean callback_treeview_testcollapse(GtkTreeView * treeview, GtkTreeIter * arg1, GtkTreePath * arg2, gpointer user_data)
{
	return (TRUE);
}

void callback_treeview_change(GtkTreeSelection * selection, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

/*
	if (nd==NULL)
		fprintf(stderr, "changed called but no selectednode\n");
	else
		fprintf(stderr, "changed called, selectednode is %d\n", nd->sql3id);
*/
#ifdef NEW_SEL_LOGIC
	guint tm=time(NULL);
	if (mainview->cansel_time>0 && mainview->cansel_time+1.0<tm) mainview->cansel_node=NULL;

	if (nd==NULL)
		{
		if (mainview->cansel_node!=NULL)
			{
			fprintf(stderr, "[SELLOGIC] saving %d to unselect all nodes\n", (mainview->cansel_node)->sql3id);
			saveDataToNode(mainview, (mainview->cansel_node));
			mainview->cansel_node=NULL;
			}
		return;
		}

	if (mainview->cansel_node!=NULL)
		{
		if (nd->sql3id == (mainview->cansel_node)->sql3id)
			{
			mainview->cansel_node=NULL;
			fprintf(stderr, "[SELLOGIC] doubly selected %d, doing nothing\n", nd->sql3id);
			return;
			}
		else
			{
			fprintf(stderr, "[SELLOGIC] saving %d to load new node\n", (mainview->cansel_node)->sql3id);
			saveDataToNode(mainview, (mainview->cansel_node));
			mainview->cansel_node=NULL;
			}
		}
#endif

	if (nd == NULL) return;

	setBusy(mainview, 1);

	gboolean goterr = TRUE;
	char *textdata = NULL;
	char *blob = NULL;
	int blobsize = 0, textsize = 0;

	char tq[512];

	snprintf(tq, sizeof(tq), "SELECT bodytype, body, bodyblob, flags FROM %s WHERE nodeid=%d", datatable_tmpname, nd->sql3id);
	sqlite3_stmt *stmt = NULL;
	const char *dum;
	int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

	if (rc)
		{
			fprintf(stderr, "Error reading (1) %s\n", sqlite3_errmsg(mainview->db));
		}
	else
		{
			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
					else if (rc == SQLITE_ROW)
						{
							nd->typ = sqlite3_column_int(stmt, 0);
							nd->flags = sqlite3_column_int(stmt, 3);

							prepareUIforNodeChange(mainview, nd->typ);
							if (nd->typ == NODE_TEXT)
								{
									gboolean file_edited_backup = mainview->file_edited;

									blobsize = sqlite3_column_bytes(stmt, 2);
									blob = (char *)sqlite3_column_blob(stmt, 2);

									textdata = (char *)sqlite3_column_text(stmt, 1);
									textsize = sqlite3_column_bytes(stmt, 1);

/*									gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), "", -1);*/
									wp_text_buffer_reset_buffer(mainview->buffer, TRUE);									

									gboolean richtext=FALSE;

									if (blob != NULL)
										{
#if 0
										gboolean oldway=FALSE;
										if (blobsize>8)
											{
											char tst[8];
											strncpy(tst, blob, 8);
											tst[8]=0;
											if (strcmp(tst, "RICHTEXT")==0) oldway=TRUE;
											}
										if (oldway)
											{
											GError *err = NULL;
											GtkTextIter iter;
											gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(mainview->buffer), &iter);
											gtk_text_buffer_deserialize_rich_text(GTK_TEXT_BUFFER(mainview->buffer), &iter, blob, blobsize, TRUE, &err);
											if (err != NULL)
												{
													fprintf(stderr, "Error loading text memo with richtext parser! %s\n", err->message);
													g_error_free(err);
												}
											else
												{
													richtext=TRUE;
												}
											}
#endif
	
										if (richtext==FALSE)
											{
											wp_text_buffer_load_document_begin(mainview->buffer, TRUE);
											wp_text_buffer_load_document_write(mainview->buffer, blob, blobsize);
											wp_text_buffer_load_document_end(mainview->buffer);
											richtext=TRUE;
											}
										}
									if (richtext==FALSE && !(textdata == NULL || g_utf8_validate(textdata, textsize, NULL) == FALSE))
										{
/*											gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), textdata, textsize);*/
										wp_text_buffer_load_document_begin(mainview->buffer, FALSE);
										wp_text_buffer_load_document_write(mainview->buffer, textdata, textsize);
										wp_text_buffer_load_document_end(mainview->buffer);
										}

									wp_text_buffer_enable_rich_text(mainview->buffer, TRUE);
									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->tools_wordwrap), ((nd->flags & NODEFLAG_WORDWRAP) > 0)?TRUE:FALSE);
									callback_wordwrap(NULL, mainview);	/*FIXME:ugly (do we need this? gotta test) */

									gtk_text_buffer_set_modified(GTK_TEXT_BUFFER(mainview->buffer), FALSE); /*we probably don't need this*/

									callback_undotoggle((gpointer)mainview->buffer, FALSE, mainview); /*we need these*/
									callback_redotoggle((gpointer)mainview->buffer, FALSE, mainview);
									
									if (file_edited_backup==FALSE) mainview->file_edited=FALSE; /*textview changed event toggles this?*/
									goterr = FALSE;
								}
							else if (nd->typ == NODE_SKETCH)
								{
									sketchwidget_wipe_undo(mainview->sk);
									sketchwidget_set_fillmode(mainview->sk, FALSE);
									sketchwidget_set_shift(mainview->sk, FALSE);
									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->shapemenuitems[1]), TRUE);
									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->shapemenuitems[0]), TRUE);

									blobsize = sqlite3_column_bytes(stmt, 2);
									blob = (char *)sqlite3_column_blob(stmt, 2);
									gboolean clear = TRUE;

									if (blob == NULL)
										goterr = FALSE;
									if (blob != NULL)
										{
											fprintf(stderr, "blobsize:%d\n", blobsize);
											GdkPixbufLoader *pl = gdk_pixbuf_loader_new_with_type("png", NULL);
											GError *err = NULL;

											gdk_pixbuf_loader_write(pl, (guchar *) blob, blobsize, &err);
											if (err != NULL)
												{
													fprintf(stderr, "Error loading sketch! %s\n", err->message);
													g_error_free(err);
													err = NULL;
												}
											gdk_pixbuf_loader_close(pl, NULL);
											GdkPixbuf *pixbuf = gdk_pixbuf_loader_get_pixbuf(pl);

											if (GDK_IS_PIXBUF(pixbuf))
												{
													GtkWidget *skdr = sketchwidget_get_drawingarea(mainview->sk);
													GtkPixmap *skpix = (GtkPixmap *) sketchwidget_get_Pixmap(mainview->sk);

													int w=gdk_pixbuf_get_width(pixbuf);
													int h=gdk_pixbuf_get_height(pixbuf);
													if (w!=skdr->allocation.width || h!=skdr->allocation.height)
														{
														if (w>skdr->allocation.width) w=skdr->allocation.width;
														if (h>skdr->allocation.height) h=skdr->allocation.height;
														sketchwidget_clear_real(mainview->sk);
														}
													gdk_draw_pixbuf(GDK_DRAWABLE(skpix), NULL, pixbuf, 0, 0, 0, 0, w, h, GDK_RGB_DITHER_NONE, 0, 0);

													clear = FALSE;
													goterr = FALSE;

													if (skpix && G_IS_OBJECT(skpix)) g_object_unref(skpix);
												}
											else
												{
													fprintf(stderr, "error loading pixbuf\n");
												}
											g_object_unref(pl);
										}
									if (clear == TRUE)
										{
											fprintf(stderr, "clearing pix area\n");
											sketchwidget_clear_real(mainview->sk);
										}
									gtk_widget_queue_draw(sketchwidget_get_drawingarea(mainview->sk));
								}
							else if (nd->typ == NODE_CHECKLIST)
								{
								gtk_object_set_data(GTK_OBJECT(mainview->listview), "edited", FALSE);
								GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
								g_object_ref(model);
							  gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->listview), NULL);
								gtk_list_store_clear(GTK_LIST_STORE(model));

								snprintf(tq, sizeof(tq), "SELECT name, style, color FROM %s WHERE nodeid=%d ORDER BY ord, idx", checklisttable_tmpname, nd->sql3id);
								sqlite3_stmt *stmt2 = NULL;
								const char *dum;
								int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt2, &dum);
								if (rc)
									{
										fprintf(stderr, "Error reading checklist (1) %s\n", sqlite3_errmsg(mainview->db));
									}
								else
									{
										goterr=FALSE;
										rc = SQLITE_BUSY;
										while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
											{
												rc = sqlite3_step(stmt2);
												if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
													break;
												else if (rc == SQLITE_ROW)
													{
														char *textdata = (char *)sqlite3_column_text(stmt2, 0);
														int style = sqlite3_column_int(stmt2, 1);
														unsigned long col = sqlite3_column_int(stmt2, 2);

														GtkTreeIter    toplevel;
														gtk_list_store_append(GTK_LIST_STORE(model), &toplevel);
														gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_TEXT, textdata, CHECKNODE_CHECKED, FALSE, -1);
														if ((style & CHECKSTYLE_CHECKED)>0) gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_CHECKED, TRUE, -1);
														if ((style & CHECKSTYLE_BOLD)>0) gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_BOLD, PANGO_WEIGHT_BOLD, -1);
														if ((style & CHECKSTYLE_STRIKE)>0) gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_STRIKE, TRUE, -1);

														if (col>0)
															{
															char tmp[10];
															snprintf(tmp, sizeof(tmp), "#%02x%02x%02x", ((col & 0xFF0000) >> 16), ((col & 0xFF00) >> 8), (col & 0xFF));
/*															fprintf(stderr, "read color for node is *%s*\n", tmp);*/
															gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_COLOR, tmp, -1);	
															}

													}
											}
										if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
											{
											fprintf(stderr, "Error reading checklist (2) %s\n", sqlite3_errmsg(mainview->db));
											goterr=TRUE;
											}
							
										sqlite3_finalize(stmt2);
									}

							  gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->listview), model);
                                                                gtk_tree_view_columns_autosize(mainview->listview);
								g_object_unref(model);
								}

							if ((nd->flags & NODEFLAG_SKETCHLINES) > 0)
								gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->sketchlinesmenuitems[1]), TRUE);
							else if ((nd->flags & NODEFLAG_SKETCHGRAPH) > 0)
								gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->sketchlinesmenuitems[2]), TRUE);
							else
								{
									gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->sketchlinesmenuitems[0]), TRUE);
									callback_sketchlines(NULL, mainview->sketchlinesmenuitems[0]);	/*FIXME:ugly */
								}
							gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->tools_pressure), TRUE);

							break;
						}
				}
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				fprintf(stderr, "Error reading (2) %s\n", sqlite3_errmsg(mainview->db));

			sqlite3_finalize(stmt);
		}

	setBusy(mainview, 2);

	if (goterr == TRUE)
		{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error loading memo"));
		}
}

gboolean treeview_canselect(GtkTreeSelection * selection, GtkTreeModel * model, GtkTreePath * path, gboolean path_currently_selected, gpointer userdata)
{
	MainView *mainview = (MainView *) userdata;
	g_assert(mainview != NULL && mainview->data != NULL);

	if (mainview->loading==FALSE)
		{
#ifndef EXPANDING_ROWS
			if (path_currently_selected)
				return (FALSE);
#endif
		}
#ifndef NEW_SEL_LOGIC
	saveCurrentData(mainview);
#else
	if (path_currently_selected)
		{
		GtkTreeIter iter;
	  gtk_tree_model_get_iter(model, &iter, path);
	
	  gtk_tree_model_get(model, &iter, NODE_DATA, &(mainview->cansel_node), -1);
		mainview->cansel_time=time(NULL);
/*  	fprintf(stderr, "[SELLOGIC] canselect called for node %d\n", nd->sql3id);*/
		}
#endif

	return (TRUE);
}

gboolean newnodedlg_key_press_cb(GtkWidget * widget, GdkEventKey * event, GtkWidget * dlg)
{
	SketchWidget *s = gtk_object_get_data(GTK_OBJECT(dlg), "sk");

	switch (event->keyval)
		{
		case GDK_F7:
			sketchwidget_redo(s);
			return TRUE;
		case GDK_F8:
			sketchwidget_undo(s);
			return TRUE;
		}
	return FALSE;
}


/* This struct will hold all our toggle buttons, so we can
 * only allow one to be active at a time (i.e. radio buttons) */
typedef struct _newNodeToggleButtons newNodeToggleButtons;
struct _newNodeToggleButtons
{
    GtkWidget *rbt; /* Text */
    GtkWidget *rbs; /* Sketch */
    GtkWidget *rbc; /* Checklist */
};

void nntb_toggled(GtkToggleButton *togglebutton, gpointer user_data)
{
    newNodeToggleButtons *nntb = (newNodeToggleButtons*)user_data;
    static gboolean toggle_in_progress = FALSE;

    /* The toggle_in_progress flag is needed, because the toggled
     * signal is emitted when setting the active property of the
     * GtkToggleButtons below - this would result in an endless
     * recursion - so we don't allow recursive toggle signals. */

    if (toggle_in_progress) return;

    toggle_in_progress = TRUE;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(nntb->rbt), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(nntb->rbs), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(nntb->rbc), FALSE);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(togglebutton), TRUE);
    toggle_in_progress = FALSE;
}

void show_sketch_widget(GtkWidget *widget, gpointer user_data)
{
    GtkWidget *dialog = (GtkWidget*)user_data;

    /* Show the sketch widget and hide the entry + draw button */
    gtk_widget_show(gtk_object_get_data(GTK_OBJECT(dialog), "al"));
    gtk_widget_hide(gtk_object_get_data(GTK_OBJECT(dialog), "draw_button"));
    gtk_widget_hide(gtk_object_get_user_data(GTK_OBJECT(dialog)));
}

void new_node_dialog(nodeType typ, MainView * mainview)
{
	GtkWidget *dialog, *label, *entry, *but_ok, *but_cancel, *vbox, *hbox, *al, *cb;
        GtkWidget *rb1, *rb2, *rb3;
        GtkWidget *bb, *im, *lb, *but_sketch, *hb;
        gchar* tmp_str;
        gchar datetime_str[200];
        time_t t_now;
        struct tm *tm_now;
        gboolean datetime_written = FALSE;

        t_now = time(NULL);
        tm_now = localtime(&t_now);
        
        if (tm_now != NULL) {
            if (strftime(datetime_str, sizeof(datetime_str), "%y-%m-%d %H:%M", tm_now) != 0) {
                datetime_written = TRUE;
            }
        }

        if (datetime_written == FALSE) {
            /* Was not able to determine a datetime string - use default */
            fprintf(stderr, "Warning: cannot resolve time!\n");
            strncpy(datetime_str, _("New memo"), sizeof(datetime_str));
            datetime_str[sizeof(datetime_str)-1] = '\0';
        }
        
        newNodeToggleButtons *nntb = g_malloc(sizeof(newNodeToggleButtons));

	dialog = gtk_dialog_new();
        gtk_window_set_title(GTK_WINDOW(dialog), _("Create new node"));
        gtk_dialog_set_has_separator(GTK_DIALOG(dialog), FALSE);

	g_signal_connect(G_OBJECT(dialog), "key_press_event", G_CALLBACK(newnodedlg_key_press_cb), dialog);

	vbox = gtk_vbox_new(FALSE, 5);

	hbox = gtk_hbox_new(TRUE, 10);

        /* Text note toggle button */
        rb1 = gtk_toggle_button_new();
        bb = gtk_hbox_new(FALSE, 5);
        gtk_container_set_border_width(GTK_CONTAINER(bb), 10);
        gtk_container_add(GTK_CONTAINER(rb1), bb);
        im = gtk_image_new_from_file(PIXMAPDIR "/text.png");
        lb = gtk_label_new(NULL);
        tmp_str = g_strdup_printf("<b>%s</b>", _("Rich text"));
        gtk_label_set_markup(GTK_LABEL(lb), tmp_str);
        g_free(tmp_str);
        gtk_box_pack_start(GTK_BOX(bb), im, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(bb), lb, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(rb1), FALSE, FALSE, 0);
        g_signal_connect(G_OBJECT(rb1), "toggled", G_CALLBACK(nntb_toggled), nntb);
        nntb->rbt = rb1;

        /* Sketch toggle button */
        rb2 = gtk_toggle_button_new();
        bb = gtk_hbox_new(FALSE, 5);
        gtk_container_set_border_width(GTK_CONTAINER(bb), 10);
        gtk_container_add(GTK_CONTAINER(rb2), bb);
        im = gtk_image_new_from_file(PIXMAPDIR "/sketch.png");
        lb = gtk_label_new(NULL);
        tmp_str = g_strdup_printf("<b>%s</b>", _("Sketch"));
        gtk_label_set_markup(GTK_LABEL(lb), tmp_str);
        g_free(tmp_str);
        gtk_box_pack_start(GTK_BOX(bb), im, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(bb), lb, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(rb2), FALSE, FALSE, 0);
        g_signal_connect(G_OBJECT(rb2), "toggled", G_CALLBACK(nntb_toggled), nntb);
        nntb->rbs = rb2;
        
        /* Checklist toggle button */
        rb3 = gtk_toggle_button_new();
        bb = gtk_hbox_new(FALSE, 5);
        gtk_container_set_border_width(GTK_CONTAINER(bb), 10);
        gtk_container_add(GTK_CONTAINER(rb3), bb);
        im = gtk_image_new_from_file(PIXMAPDIR "/checklist.png");
        lb = gtk_label_new(NULL);
        tmp_str = g_strdup_printf("<b>%s</b>", _("Checklist"));
        gtk_label_set_markup(GTK_LABEL(lb), tmp_str);
        g_free(tmp_str);
        gtk_box_pack_start(GTK_BOX(bb), im, FALSE, FALSE, 0);
        gtk_box_pack_start(GTK_BOX(bb), lb, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(rb3), FALSE, FALSE, 0);
        g_signal_connect(G_OBJECT(rb3), "toggled", G_CALLBACK(nntb_toggled), nntb);
        nntb->rbc = rb3;

        /* Add padding around our toggle buttons */
        gtk_container_set_border_width(GTK_CONTAINER(hbox), 5);
	
        /* Remember "new note toggle buttons" list */
        gtk_object_set_data(GTK_OBJECT(dialog), "nntb", nntb);
        
	if (typ == NODE_TEXT) {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb1), TRUE);
        } else if (typ == NODE_SKETCH) {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb2), TRUE);
        } else {
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(rb3), TRUE);
        }

        gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);

	but_ok = gtk_button_new_with_label(_("Create node"));
	but_cancel = gtk_button_new_with_label(_("Cancel"));
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_ok);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_cancel);

	gtk_object_set_data(GTK_OBJECT(dialog), "m", mainview);

	gtk_signal_connect_object(GTK_OBJECT(but_cancel), "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy), dialog);
	g_signal_connect(G_OBJECT(but_ok), "clicked", G_CALLBACK(callback_new_node_real), dialog);

        hb = gtk_hbox_new(FALSE, 10);
        gtk_box_pack_start(GTK_BOX(hb), gtk_label_new(_("Label:")), FALSE, FALSE, 0);
	entry = gtk_entry_new_with_max_length(64);
	gtk_object_set_user_data(GTK_OBJECT(dialog), entry);
	gtk_entry_set_text(GTK_ENTRY(entry), datetime_str);
        gtk_editable_select_region(GTK_EDITABLE(entry), 0, -1);
        gtk_box_pack_start(GTK_BOX(hb), entry, TRUE, TRUE, 0);
        
        /* Sketch widget, hidden by default */
	al = gtk_alignment_new(0.5, 0.5, 0, 0);
	SketchWidget *s = sketchwidget_new(SKETCHNODE_X, SKETCHNODE_Y, TRUE);
	gtk_object_set_data(GTK_OBJECT(dialog), "sk", s);
	gtk_object_set_data(GTK_OBJECT(dialog), "al", al);
	sketchwidget_set_brushsize(s, 2);
	sketchwidget_set_backstyle(s, SKETCHBACK_GRAPH);
	gtk_widget_set_size_request(sketchwidget_get_mainwidget(s), SKETCHNODE_X, SKETCHNODE_Y);
	gtk_container_add(GTK_CONTAINER(al), sketchwidget_get_mainwidget(s));
	gtk_box_pack_start(GTK_BOX(hb), al, FALSE, FALSE, 0);

        but_sketch = gtk_button_new_with_label(_("Draw..."));
	gtk_object_set_data(GTK_OBJECT(dialog), "draw_button", but_sketch);
        gtk_signal_connect(GTK_OBJECT(but_sketch), "clicked", G_CALLBACK(show_sketch_widget), dialog);
            
        gtk_box_pack_start(GTK_BOX(hb), but_sketch, FALSE, TRUE, 0);
        gtk_box_pack_start(GTK_BOX(vbox), hb, TRUE, FALSE, 0);

	cb = gtk_check_button_new_with_label(_("Create as child node of current node"));
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cb), mainview->newnodedialog_createchild);
	gtk_container_add(GTK_CONTAINER(vbox), cb);
	gtk_object_set_data(GTK_OBJECT(dialog), "cb", cb);

        gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), vbox);
        
	gtk_widget_grab_focus(entry);

	gtk_widget_show_all(dialog);

        /* Hide the sketch widget at first */
        gtk_widget_hide(al);
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
}

void add_new_node(nodeData * node, MainView * mainview, gboolean ischild)
{
	GtkTreeIter parentiter, newiter;
	GtkTreeModel *model;
	void *ptr = NULL;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (gtk_tree_selection_get_selected(selection, &model, &parentiter))
		ptr = &parentiter;

	GtkTreePath *path = NULL;

	unsigned int parentnodeid = 0;

	if (ptr != NULL)
		{
			path = gtk_tree_model_get_path(model, &parentiter);

			if (ischild == FALSE)
				{
					gtk_tree_path_up(path);

					if (gtk_tree_path_get_depth(path) == 0)
						{
					    /* Selected node is a root node */
							ptr = NULL;   /* New node can not have a Parent node */
							gtk_tree_path_down(path);	/*restore path so expand() works */
						}
					else if (gtk_tree_path_get_depth(path) > 0)
						{
					    /* Selected node is a child node */
							if (gtk_tree_model_get_iter(model, &parentiter, path))
								ptr = &parentiter;
						}
				}
		}

	if (ptr != NULL)
		{
			nodeData *nd;

			gtk_tree_model_get(model, ptr, NODE_DATA, &nd, -1);
			if (nd)
				parentnodeid = nd->sql3id;
		}

	node->sql3id = 0;
	do
		{
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			char tq[512];

			/*
			 * FIXME: ord 
			 */
			snprintf(tq, sizeof(tq), "INSERT INTO %s (parent, bodytype, name, nameblob, ord) VALUES (%d, %d, ?, ?, 0);", datatable_tmpname, parentnodeid, node->typ);
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);

			if (rc)
				{
					fprintf(stderr, "Error inserting(1): %s\n", sqlite3_errmsg(mainview->db));
					break;
				}
			if (node->name != NULL)
				sqlite3_bind_text(stmt, 1, node->name, strlen(node->name), SQLITE_TRANSIENT);
			else
				sqlite3_bind_text(stmt, 1, NULL, 0, SQLITE_TRANSIENT);

			if (node->namepix != NULL)
				{
					gchar *namepixdata = NULL;
					gsize datalen = 0;

					GError *err = NULL;

					if (gdk_pixbuf_save_to_buffer(node->namepix, &namepixdata, &datalen, "png", &err, NULL) == FALSE)
						{
							namepixdata = NULL;
							datalen = 0;
							fprintf(stderr, "Error saving name! %s\n", err->message);
							g_error_free(err);
						}
					sqlite3_bind_blob(stmt, 2, namepixdata, datalen, SQLITE_TRANSIENT);
				}
			else
				sqlite3_bind_blob(stmt, 2, NULL, 0, SQLITE_TRANSIENT);

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
				}
			sqlite3_finalize(stmt);
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					fprintf(stderr, "Error inserting(2): %s\n", sqlite3_errmsg(mainview->db));
					break;
				}
			node->sql3id = sqlite3_last_insert_rowid(mainview->db);
		}
	while(FALSE);

	if (node->sql3id == 0)
		{
			if (node->name)
				g_free(node->name);
			if (node->namepix)
				g_object_unref(node->namepix);
			g_free(node);
			if (path)
				gtk_tree_path_free(path);
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error creating node");
			return;
		}

	gtk_tree_store_append(GTK_TREE_STORE(model), &newiter, ptr);

	gtk_tree_store_set(GTK_TREE_STORE(model), &newiter, NODE_NAME, node->name, NODE_PIXBUF, node->namepix, NODE_DATA, node, -1);

	if (path)
		{
			mainview->loading=TRUE; /*only when we have a valid parent*/
			gtk_tree_view_expand_row(GTK_TREE_VIEW(mainview->treeview), path, FALSE);
			gtk_tree_path_free(path);
		}

	gtk_tree_selection_select_iter(selection, &newiter);

	mainview->loading=FALSE;
}

void callback_new_node_real(GtkAction * action, gpointer data)
{
	MainView *mainview;

	GtkWidget *dialog = data;
	GtkWidget *entry = gtk_object_get_user_data(GTK_OBJECT(dialog));

	mainview = gtk_object_get_data(GTK_OBJECT(dialog), "m");
	SketchWidget *s = gtk_object_get_data(GTK_OBJECT(dialog), "sk");
	GtkWidget *cb = gtk_object_get_data(GTK_OBJECT(dialog), "cb");
        newNodeToggleButtons *nntb = gtk_object_get_data(GTK_OBJECT(dialog), "nntb");

	nodeType typ = NODE_TEXT;
        if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(nntb->rbs))) {
            typ = NODE_SKETCH;
        } else if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(nntb->rbc))) {
            typ = NODE_CHECKLIST;
        }
        g_free(nntb);

	gchar *txt = NULL;
	GdkPixbuf *pixbuf = NULL;

	if (GTK_WIDGET_VISIBLE(entry))
		{
			txt = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));
			if (strcmp(txt, "") == 0)
				{
					g_free(txt);
					return;
				}
		}
	else
		{
			GtkWidget *sdr = sketchwidget_get_drawingarea(s);

			GdkPixmap *spix = sketchwidget_get_Pixmap(s);

			pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(spix), NULL, 0, 0, 0, 0, sdr->allocation.width, sdr->allocation.height);
			g_object_unref(spix);
			double w, h;
			GdkPixbuf *pixbuf2 = sketchwidget_trim_image(pixbuf, SKETCHNODE_X, SKETCHNODE_Y, &w,
																									 &h, TRUE);
			if (pixbuf2==NULL) return;

			GdkPixbuf *pixbuf3 = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, SKETCHNODE_RX,
																					SKETCHNODE_RY);

			gdk_pixbuf_fill(pixbuf3, 0xffffffff);

			if (w <= SKETCHNODE_RX && h <= SKETCHNODE_RY)
				{
					gdk_pixbuf_copy_area(pixbuf2, 0, 0, w, h, pixbuf3, 0, (SKETCHNODE_RY - h) / 2);
				}
			else
				{
					double neww, newh;

					if (w > h)
						{
							neww = SKETCHNODE_RX;
							newh = (h / w) * SKETCHNODE_RX;
						}
					else
						{
							newh = SKETCHNODE_RY;
							neww = (w / h) * SKETCHNODE_RY;
						}
					if (newh > SKETCHNODE_RY)
						newh = SKETCHNODE_RY;

					GdkPixbuf *tmpbuf = gdk_pixbuf_scale_simple(pixbuf2, neww, newh,
																											GDK_INTERP_BILINEAR);

					gdk_pixbuf_copy_area(tmpbuf, 0, 0, neww, newh, pixbuf3, 0, (SKETCHNODE_RY - newh) / 2);

					gdk_pixbuf_unref(tmpbuf);
				}

			pixbuf = pixbuf3;
			gdk_pixbuf_unref(pixbuf2);
		}

	nodeData *node;

	node = g_malloc(sizeof(nodeData));
	node->typ = typ;
	node->name = NULL;
	node->namepix = NULL;

	if (GTK_WIDGET_VISIBLE(entry))
		{
			node->name = txt;
		}
	else
		{
			node->namepix = pixbuf;
		}

	node->lastMod = 0;
	node->flags = 0;
	node->sql3id = 0;

	mainview->newnodedialog_createchild = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(cb));
	add_new_node(node, mainview, mainview->newnodedialog_createchild);

	sketchwidget_destroy(s);
	gtk_widget_destroy(dialog);
	mainview->file_edited = TRUE;
}

/*
 * delete node 
 */
void callback_file_delete_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	if (getSelectedNode(mainview) == NULL)
		{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Select a node first"));
			return;
		}

	GtkWidget *dialog, *label, *but_ok, *but_cancel;

	dialog = gtk_dialog_new();

	label = gtk_label_new(_("Delete current memo?"));

	but_ok = gtk_button_new_with_label(_("Yes, Delete"));
	but_cancel = gtk_button_new_with_label(_("Cancel"));

	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_ok);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_cancel);

	gtk_object_set_user_data(GTK_OBJECT(dialog), mainview);

	gtk_signal_connect_object(GTK_OBJECT(but_cancel), "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy), dialog);
	g_signal_connect(G_OBJECT(but_ok), "clicked", G_CALLBACK(callback_delete_node_real), dialog);

	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), label);

	gtk_widget_show_all(dialog);
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
}

/*
 * Callback for Rename Menuitem 
 */
void callback_file_rename_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	/* Get the selected node */
	nodeData *sel_node = getSelectedNode(mainview);
	if (!sel_node)
		{
		  /* Do nothing, if no node has been selected */
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Select a node first"));
			return;
		}
        
	if (sel_node->namepix != NULL)
	{
		/* the memo has a graphical label, cannot edit! */
		hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Cannot rename memos with sketch name"));
		return;
	}

	/* Create dialog box */
	GtkWidget *dialog, *label, *entry, *but_ok, *but_cancel;

	dialog = gtk_dialog_new();

	label = gtk_label_new(_("New Memo name:"));

	entry = gtk_entry_new_with_max_length(64);
	gtk_entry_set_text (GTK_ENTRY(entry), sel_node->name);
	gtk_editable_select_region (GTK_EDITABLE(entry), 0, -1);

	but_ok = gtk_button_new_with_label(_("OK"));
	but_cancel = gtk_button_new_with_label(_("Cancel"));

	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_ok);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_cancel);

	g_object_set_data(G_OBJECT(dialog), "m", mainview);
	g_object_set_data(G_OBJECT(dialog), "e", entry);

	gtk_signal_connect_object(GTK_OBJECT(but_cancel), "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy), dialog);
	g_signal_connect(G_OBJECT(but_ok), "clicked", G_CALLBACK(callback_rename_node_real), dialog);

	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), label);
	gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), entry);

	gtk_box_set_spacing (GTK_DIALOG(dialog)->vbox, 10);
	
	gtk_widget_show_all(dialog);
	gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
}

/*
 * Callback for Rename Dialog
 */
void callback_rename_node_real (GtkAction *action, gpointer data)
{
  MainView *mainview;
	GtkWidget *entry;
	GtkTreeIter iter;
	GtkTreeModel *model;
	nodeData *nd = NULL;

  GtkWidget *dialog = data;

  mainview = g_object_get_data (G_OBJECT(dialog), "m");	
	entry = g_object_get_data (G_OBJECT(dialog), "e");

  /* Checking whether the entry is empty */
	gchar *new_node_name = gtk_entry_get_text (GTK_ENTRY(entry));
	if (strcmp (new_node_name, "") == 0) {
		hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Name can not be empty"));
		return; 
	}
	
	/* Get the selected node */
	GtkTreeSelection *selection = gtk_tree_view_get_selection (GTK_TREE_VIEW(mainview->treeview));

	if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    return;

	gtk_tree_model_get (model, &iter, NODE_DATA, &nd, -1);
	if (!nd) 
		return;

	/* If we reach here, all checkings are done */
  /* Update the database */
	char tq[512];
	unsigned int sql3id = nd->sql3id;
	sqlite3_stmt *stmt = NULL;
	const char* dum;
	gboolean db_query_result = FALSE;

	/* Update the noteData */
	if (nd->name) {
	    g_free(nd->name);
	}
	nd->name = g_strdup(new_node_name);
	
	do {
    snprintf (tq, sizeof(tq), "UPDATE %s SET name=\"%s\" WHERE nodeid=%d", 
																datatable_tmpname, new_node_name, sql3id);
	  int rc = sqlite3_prepare (mainview->db, tq, strlen(tq), &stmt, &dum);

	  if (rc) {
      fprintf (stderr, "Error preparing db update query: %s\n", sqlite3_errmsg(mainview->db));
		  break;
	  }
		rc = SQLITE_BUSY;
		while (rc == SQLITE_BUSY) {
		  rc = sqlite3_step (stmt);
			if (rc == SQLITE_DONE) {
        db_query_result = TRUE;
			  break;
			}
		  else if(rc == SQLITE_ERROR || rc== SQLITE_MISUSE) {
        fprintf (stderr, "Error updating node: %s\n", sqlite3_errmsg(mainview->db));
				break;
			}
			sqlite3_finalize(stmt);
		}
	} while (FALSE);

	/* Update the tree */
	if (db_query_result)
	  gtk_tree_store_set (GTK_TREE_STORE(model), &iter, NODE_NAME, new_node_name, -1);
  
	mainview->file_edited = TRUE;

	/* Destroy the dialog */
	gtk_widget_destroy (dialog);
}

void callback_file_expand_collapse_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeIter iter;
	GtkTreeModel *model;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (!gtk_tree_selection_get_selected(selection, &model, &iter))
		{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Select a node first"));
			return;
		}

	GtkTreePath *path = gtk_tree_model_get_path(model, &iter);
	if (gtk_tree_view_row_expanded(GTK_TREE_VIEW(mainview->treeview), path)==FALSE)
		{
		gtk_tree_view_expand_row(GTK_TREE_VIEW(mainview->treeview), path, FALSE);
		}
	else
		{
		gtk_tree_view_collapse_row(GTK_TREE_VIEW(mainview->treeview), path);
		}

	gtk_tree_path_free(path);
}

void callback_file_export_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd=getSelectedNode(mainview);
	if (nd == NULL)
		{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Select a memo first"));
			return;
		}

	gchar *nodename=nd->name;
	if (nodename==NULL) nodename=_("saved memo");

	if (nd->typ == NODE_TEXT)
		{
/*
		GtkTextIter begin, end;
		gtk_text_buffer_get_bounds (GTK_TEXT_BUFFER(mainview->buffer), &begin, &end);
		gchar *text = gtk_text_buffer_get_slice(GTK_TEXT_BUFFER(mainview->buffer), &begin, &end, TRUE);
*/
			GString *gstr=g_string_sized_new(4096);
			wp_text_buffer_save_document(mainview->buffer, (WPDocumentSaveCallback)(wp_savecallback), gstr);
			gint textlen=gstr->len;
			gchar *text=g_string_free(gstr, FALSE);

		if (text==NULL || !strcmp(text, ""))
			{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Memo is empty"));
			}
		else
			{
			gchar *fn = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, nodename, "html");
			if (fn!=NULL)
				{
				GnomeVFSResult vfs_result;
				GnomeVFSHandle *handle = NULL;
				GnomeVFSFileSize out_bytes;
  			vfs_result = gnome_vfs_create(&handle, fn, GNOME_VFS_OPEN_WRITE, 0, 0600);
		    if ( vfs_result != GNOME_VFS_OK ) {
					hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Export failed"));
		    	}
    		else
    			{
    			gnome_vfs_write(handle, text, textlen, &out_bytes);
    			gnome_vfs_close(handle);
    			if (out_bytes==strlen(text)) hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Exported"));
    															else hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Export incomplete"));
    			}
				g_free(fn);
				}
			}
		g_free(text);
		}
	else if (nd->typ == NODE_SKETCH)
		{
		GdkPixmap *skpix = sketchwidget_get_Pixmap(mainview->sk);
		GtkWidget *skdr = sketchwidget_get_drawingarea(mainview->sk);
		GdkPixbuf *pixbuf = gdk_pixbuf_get_from_drawable(NULL, GDK_DRAWABLE(skpix), NULL, 0, 0, 0, 0, skdr->allocation.width, skdr->allocation.height);
		if (pixbuf==NULL)
			{
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Memo is empty"));
			}		
		else
			{
			gchar *fn = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, nodename, "png");
			if (fn!=NULL)
				{
				if (gdk_pixbuf_save(pixbuf, fn, "png", NULL, NULL)==FALSE)
					{
					hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Export failed"));
					}
				else
					{
					hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Exported"));
					}
				g_free(fn);
				}
			}
		g_object_unref(skpix);
		}
	else if (nd->typ == NODE_CHECKLIST)
		{
					hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Unimplemented"));
		}
}

/*
 *	callback from menu item
 *	move selected node down (switch node with next sibling), don't change level of node
 */
void callback_move_down_node(GtkAction * action, gpointer data)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
	gtk_tree_selection_get_selected(selection, &model, &iter);

	GtkTreeIter old_iter = iter;/*save pointer to old iter, we will need it during swap nodes*/

	if (gtk_tree_model_iter_next(model,&iter)==FALSE)/*get next node*/
		return;

	GtkTreeStore *treeStore = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview)));
	gtk_tree_store_swap(treeStore,&iter,&old_iter);

	mainview->file_edited = TRUE;/*we have made changes , if required show "save changes?" dialog in future*/
}

/*
 *	callback from menu item
 *	move selected node down (switch node with prev sibling), don't change level of node
 */
void callback_move_up_node(GtkAction * action, gpointer data)
{
	GtkTreeIter iter;
	GtkTreeModel *model;

	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
	gtk_tree_selection_get_selected(selection, &model, &iter);

	GtkTreeIter old_iter=iter;/*save pointer to old iter, we will need it during swap nodes*/

	if (tree_model_iter_prev(model,&iter)==FALSE)/*get previous node*/
		return;

	GtkTreeStore *treeStore = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview)));
	gtk_tree_store_swap(treeStore,&old_iter,&iter);/*do move*/

	mainview->file_edited = TRUE;/*we have made changes , if required show "save changes?" dialog in future*/
}

/*
 *	callback from menu item
 *	we change level of actual node with direction to top
 */
void callback_move_to_top_level_node(GtkAction * action, gpointer data)
{
	GtkTreeIter iter,new_parent;
	GtkTreeIter *p_new_parent;
	GtkTreeIter parent;
	GtkTreeModel *model;
	
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
	gtk_tree_selection_get_selected(selection, &model, &iter);
	
	/*at first we need actual parent of selected node*/
	if (gtk_tree_model_iter_parent(model,&parent,&iter)==FALSE)
	{
		/*if parent of selected node is ROOT we can't go higher*/
		return;
	}
	/*we need also new parent, it's parent of actual parent*/
	if (gtk_tree_model_iter_parent(model,&new_parent,&parent)==FALSE)
	{
		/*if our new parent is ROOT we got filled new_parent with invalid value,
		 so we need set NULL value to p_new_parent (root item)*/
		p_new_parent=NULL;
	}
	else
	{
		p_new_parent=&new_parent;/*we only redirect pointer to treeiter*/
	}

	saveCurrentData(mainview);/*we save changes in node befor move*/
		
	/*this move function provide move item with all his children, be careful iter value will change!*/
 	if (move_node(mainview,p_new_parent,&iter,&parent)==TRUE){
		
		gint id_parent = get_node_id_on_tmp_db(model,p_new_parent);
		gint id_node = get_node_id_on_tmp_db(model,&iter);	
		/*we need also update parent id of moved item*/
		char tq[512];
		snprintf (tq, sizeof(tq), "UPDATE %s SET parent=%d WHERE nodeid=%d",datatable_tmpname,id_parent ,id_node);
		exec_command_on_db(mainview,tq);
		
		/*select new created iter*/
		gtk_tree_selection_select_iter(selection,&iter);

		mainview->file_edited = TRUE;/*we have made changes , if required show "save changes?" dialog in future*/
	}
}

/*
 *	callback from menu item
 *	we change level of actual node with direction to bottom
 * 	previous node will be parent of our actual node
 */
void callback_move_to_bottom_level_node(GtkAction * action, gpointer data)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
	gtk_tree_selection_get_selected(selection, &model, &iter);

	GtkTreeIter move_iter=iter;/*save pointer to old iter*/
	
	/*we try to get previous node*/
	if (tree_model_iter_prev(model,&iter)==FALSE)
		return;/*if previous node on the same level doesn't exist we will exit*/

	saveCurrentData(mainview);/*we save changes in node befor move*/

	/*this move function provide move item with all his children, be careful move_iter value will change!*/
	if (move_node(mainview,&iter,&move_iter,NULL)==TRUE)
	{
		gint id_parent = get_node_id_on_tmp_db(model,&iter);
		gint id_node = get_node_id_on_tmp_db(model,&move_iter);

		/*we need also update parent id of moved item*/
		char tq[512];
		snprintf (tq, sizeof(tq), "UPDATE %s SET parent=%d WHERE nodeid=%d",datatable_tmpname,id_parent ,id_node);
		exec_command_on_db(mainview,tq);

		GtkTreePath *path = gtk_tree_model_get_path(model, &iter);
		gtk_tree_view_expand_row(GTK_TREE_VIEW(mainview->treeview), path, FALSE);/*expand parent node*/
		gtk_tree_path_free(path);
	
		/*select new created iter*/
		gtk_tree_selection_select_iter(selection,&move_iter);
	
		mainview->file_edited = TRUE;/*we have made changes , if required show "save changes?" dialog in future*/
	}
}

/*
 *	move item_to_move to new_parent with his children, this function is designed for change level of node
 *	we copy item_to_move with his children to new position (after item_befor if is not NULL) and then we 
 *	destroy old node with his children, so ! item_to_move is set with new iter, be careful on this!
 */
gboolean move_node(MainView *mainview,GtkTreeIter *new_parent,GtkTreeIter *item_to_move,GtkTreeIter *item_befor)
{
	GtkTreeModel *model;
	
	model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview));
	GtkTreeStore *treeStore = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview)));
	
	nodeData *node;
	gtk_tree_model_get(model, item_to_move, NODE_DATA, &node, -1);/*get data from actual iter*/
	if (node)
	{
		GtkTreeIter new_iter;/*create new iter*/
		gtk_tree_store_append(treeStore,&new_iter,new_parent);/*append new iter to new parent*/

		if (item_befor!=NULL)
			gtk_tree_store_move_after(treeStore,&new_iter,item_befor);/*sometimes we need set position*/

		gtk_tree_store_set(treeStore, &new_iter, NODE_NAME, node->name,NODE_PIXBUF, node->namepix, NODE_DATA, node, -1);/*set data from old iter*/
		
		GtkTreeIter child;
		while (gtk_tree_model_iter_children(model, &child, item_to_move)==TRUE)/*move all childrens while some exits*/
		{
			if (move_node(mainview,&new_iter,&child,NULL)==FALSE)/*use recursion on children*/
				return FALSE;
		}
		
		gtk_tree_store_set(treeStore, item_to_move, NODE_DATA, NULL, -1);
		gtk_tree_store_remove(treeStore, item_to_move);/*remove node, data need't remove, they are stored in new node*/	
		
		/*we need return new value of moved item, so we need assign new_iter to item_to_move*/
		/*this code is ugly : new_iter to path and back to item_to_move*/
		GtkTreePath *path=gtk_tree_model_get_path(model,&new_iter);
		gtk_tree_model_get_iter(model,item_to_move,path);
		gtk_tree_path_free(path);
	}
	else
	{
		fprintf(stderr,"Get data node failed!\n");
		return FALSE;
	}
	
	return TRUE;
}

/*
 *	simple execute of sql command which is stored in sql_string[]
 */
gboolean exec_command_on_db(MainView *mainview,char sql_string[])
{
	sqlite3_stmt *stmt = NULL;
	const char* dum;
	gboolean db_query_result = FALSE;

	int rc = sqlite3_prepare (mainview->db, sql_string, strlen(sql_string), &stmt, &dum);
	
	if (rc) {
		fprintf (stderr, "Error preparing db update query: %s\n", sqlite3_errmsg(mainview->db));
		return FALSE;
	}

	rc = SQLITE_BUSY;
	while (rc == SQLITE_BUSY) {
		rc = sqlite3_step (stmt);
		if (rc == SQLITE_DONE) {
			db_query_result = TRUE;
			break;
		}
		else if(rc == SQLITE_ERROR || rc== SQLITE_MISUSE) {
			fprintf (stderr, "Error updating node: %s\n", sqlite3_errmsg(mainview->db));
			break;
		}
		sqlite3_finalize(stmt);
	}	
	return TRUE;
}

/*
 *	it is used in gtk_tree_model_foreach function to update ord value for all nodes (it's usefull for move up, move down node)
 */
gboolean foreach_func_update_ord (GtkTreeModel *model,GtkTreePath *path,GtkTreeIter *iter, MainView *mainview)
{
	nodeData *node;	
	gtk_tree_model_get(model, iter, NODE_DATA, &node, -1);
	/*we need index of node on actual level*/
	gint index=get_branch_node_index(path);

	/*prepare to execute update command,and exec it*/
	char sql_command[512];
	snprintf (sql_command, sizeof(sql_command), "UPDATE %s SET ord=\"%d\" WHERE nodeid=%d",datatable_tmpname, index, node->sql3id);
	exec_command_on_db(mainview,sql_command);

	/*we don't want break gtk_tree_model_foreach function,until we call this func on each node -  so we return always FALSE*/
	return FALSE; 
}

/*
 *	return id number of iter (id number which is used to identify in sql database of nodes)
 */
int get_node_id_on_tmp_db(GtkTreeModel *model,GtkTreeIter *iter)
{
	if (iter==NULL) 
		return 0;/*we got ROOT parent here*/

	nodeData *node;
	gtk_tree_model_get(model, iter, NODE_DATA, &node, -1);
	return node->sql3id;
}

/*
 *	get index of node in current branch
 */
gint get_branch_node_index(GtkTreePath *path)
{
	int depth=gtk_tree_path_get_depth(path);
	gint *indicies = gtk_tree_path_get_indices(path);
	
	return indicies[depth-1];
}

/*
 *	similiar with gtk_tree_model_iter_next (), but opposite
 */
gboolean tree_model_iter_prev(GtkTreeModel *tree_model,GtkTreeIter *iter)
{
	GtkTreePath *path = gtk_tree_model_get_path(tree_model, iter);
	
	if (path==NULL){
		fprintf(stderr,"Error: path is null\n");
		return FALSE;
	}

	if (gtk_tree_path_prev(path)==FALSE)
		return FALSE;
	
	gtk_tree_model_get_iter(tree_model, iter,path);

	return TRUE;
}

gboolean ref2iter(GtkTreeModel * model, GtkTreeRowReference * ref, GtkTreeIter * iter)
{
	gboolean res = FALSE;
	GtkTreePath *path = gtk_tree_row_reference_get_path(ref);

	if (gtk_tree_model_get_iter(model, iter, path))
		{
			res = TRUE;
		}
	gtk_tree_path_free(path);
	return (res);
}

GtkTreeRowReference *iter2ref(GtkTreeModel * model, GtkTreeIter * iter)
{
	GtkTreeRowReference *ref;

	GtkTreePath *path = gtk_tree_model_get_path(model, iter);

	ref = gtk_tree_row_reference_new(model, path);
	gtk_tree_path_free(path);
	return (ref);
}

void move_nodes_up(GtkTreeModel * model, GtkTreeRowReference * topnode, GtkTreeRowReference * newtop)
{
	GtkTreeIter topiter;

	fprintf(stderr, "here2\n");

	if (ref2iter(model, topnode, &topiter) == FALSE)
		return;

	fprintf(stderr, "here3\n");

	GtkTreeIter child;

	if (gtk_tree_model_iter_children(model, &child, &topiter))
		{
			fprintf(stderr, "here4\n");
			GtkTreeRowReference *ref;
			GList *rr_list = NULL, *node;

			do
				{
					ref = iter2ref(model, &child);
					rr_list = g_list_append(rr_list, ref);
				}
			while(gtk_tree_model_iter_next(model, &child));

			/*
			 * got a reflist for all children
			 */

			fprintf(stderr, "here5\n");
			for(node = rr_list; node; node = node->next)
				{
					ref = (GtkTreeRowReference *) (node->data);
					if (ref2iter(model, ref, &child))
						{
							GtkTreeIter newtopiter, newiter;
							GtkTreeIter *newtopiterptr;

							if (ref2iter(model, newtop, &newtopiter))
								newtopiterptr = &newtopiter;
							else
								newtopiterptr = NULL;

							nodeData *node;

							gtk_tree_model_get(model, &child, NODE_DATA, &node, -1);

							gtk_tree_store_append(GTK_TREE_STORE(model), &newiter, newtopiterptr);
							gtk_tree_store_set(GTK_TREE_STORE(model), &newiter, NODE_NAME, node->name, NODE_PIXBUF, node->namepix, NODE_DATA, node, -1);

							GtkTreeRowReference *newref = iter2ref(model, &newiter);

							move_nodes_up(model, ref, newref);
							gtk_tree_row_reference_free(newref);

							gtk_tree_store_remove(GTK_TREE_STORE(model), &child);
						}
					gtk_tree_row_reference_free(ref);
				}
			fprintf(stderr, "here6\n");

			g_list_free(rr_list);
		}

	fprintf(stderr, "here7\n");

}

void callback_delete_node_real(GtkAction * action, gpointer data)
{
	MainView *mainview;

	GtkWidget *dialog = data;

	mainview = gtk_object_get_user_data(GTK_OBJECT(dialog));

	GtkTreeIter iter;
	GtkTreeModel *model;

	GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));

	if (!gtk_tree_selection_get_selected(selection, &model, &iter))
		return;

	nodeData *nd;

	gtk_tree_model_get(model, &iter, NODE_DATA, &nd, -1);
	if (!nd)
		return;

	mainview->file_edited = TRUE;

	unsigned int sql3id = nd->sql3id;

	if (nd->name)
		g_free(nd->name);

	/*
	 * g_free(nd->data);
	 * if (nd->pix) g_object_unref(nd->pix);
	 */
	g_free(nd);

	fprintf(stderr, "here1\n");
	GtkTreeRowReference *upref = NULL, *ref = NULL;

	GtkTreePath *path = gtk_tree_model_get_path(model, &iter);

	ref = gtk_tree_row_reference_new(model, path);
	if (gtk_tree_path_up(path))
		upref = gtk_tree_row_reference_new(model, path);
	gtk_tree_path_free(path);

	g_object_ref(model);
	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), NULL);

	fprintf(stderr, "here! 1\n");
	move_nodes_up(model, ref, upref);

	fprintf(stderr, "here! 2\n");
	if (ref2iter(model, ref, &iter))
		{
			char tq[512];

			snprintf(tq, sizeof(tq), "SELECT parent FROM %s WHERE nodeid=%d", datatable_tmpname, sql3id);
			sqlite3_stmt *stmt = NULL;
			const char *dum;
			int rc = sqlite3_prepare(mainview->db, tq, strlen(tq), &stmt, &dum);
			unsigned int sql3parentid = 0;

			if (rc)
				{
					fprintf(stderr, "Error %s\n", sqlite3_errmsg(mainview->db));
				}
			else
				{
					rc = SQLITE_BUSY;
					while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
						{
							rc = sqlite3_step(stmt);
							if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
								break;
							else if (rc == SQLITE_ROW)
								{
									sql3parentid = sqlite3_column_int(stmt, 0);
									break;
								}
						}
					sqlite3_finalize(stmt);

					snprintf(tq, sizeof(tq), "UPDATE %s SET parent=%d WHERE parent=%d;", datatable_tmpname, sql3parentid, sql3id);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						fprintf(stderr, "ERROR moving nodes up!\n");
					else
						{
							snprintf(tq, sizeof(tq), "DELETE FROM %s WHERE nodeid=%d;", datatable_tmpname, sql3id);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
								fprintf(stderr, "ERROR deleting node!\n");

                                                        /* Delete all checklist items that do not have
                                                         * a node anymore (= orphaned checklist items) */
							snprintf(tq, sizeof(tq), "DELETE FROM %s WHERE nodeid NOT IN (SELECT nodeid FROM %s);", checklisttable_tmpname, datatable_tmpname);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
								fprintf(stderr, "ERROR deleting orphaned checklist items!\n");
						}
				}
			gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
		}

	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), model);
        
        if (gtk_tree_model_get_iter_first(model, &iter)) {
            /* Select the first item, so the view is updated */
            gtk_tree_selection_select_iter(selection, &iter);
        } else {
            /* TODO: Handle case when the last memo is deleted */
        }
        
	g_object_unref(model);

	fprintf(stderr, "here! 3\n");
	gtk_tree_row_reference_free(ref);
	gtk_tree_row_reference_free(upref);

	gtk_tree_view_expand_all(GTK_TREE_VIEW(mainview->treeview));

	fprintf(stderr, "here10\n");
	gtk_widget_destroy(dialog);
}

void callback_edit_clear(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT) gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), "", 0);
	else if (nd->typ == NODE_SKETCH) sketchwidget_clear(mainview->sk);
	else if (nd->typ == NODE_CHECKLIST) gtk_list_store_clear(GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview))));
}

/*
 * cut 
 */
void callback_edit_cut(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT)
		gtk_text_buffer_cut_clipboard(GTK_TEXT_BUFFER(mainview->buffer), mainview->clipboard, TRUE);
	else if (nd->typ == NODE_SKETCH)
		{
		if (sketchwidget_cut(mainview->sk, mainview->clipboard)==FALSE)
								hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error cutting"));
		}								
	else if (nd->typ == NODE_CHECKLIST)
					hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Unimplemented"));

}

/*
 * copy 
 */
void callback_edit_copy(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT)
		gtk_text_buffer_copy_clipboard(GTK_TEXT_BUFFER(mainview->buffer), mainview->clipboard);
	else if (nd->typ == NODE_SKETCH)
		{
		if (sketchwidget_copy(mainview->sk, mainview->clipboard)==FALSE)
					hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error copying"));
		}
	else if (nd->typ == NODE_CHECKLIST)
        {
            /* Copy all selected entries as multiline text (1 line per entry) */
            GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
            GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview));
            
            gint selected_rows = gtk_tree_selection_count_selected_rows(selection);
            GList* l = gtk_tree_selection_get_selected_rows(selection, NULL);
            
            GtkTreeIter iter;
            gchar *str_data;
            
            gchar **entries = g_malloc0(sizeof(gchar*)*selected_rows+1);
            gint entries_idx = 0;
            
            GList* cur = l;
            while(cur) {
            	GtkTreePath *path = cur->data;
            
            	if (gtk_tree_model_get_iter(model, &iter, path)) {
                     gtk_tree_model_get(GTK_LIST_STORE(model), &iter, CHECKNODE_TEXT, &(entries[entries_idx++]), -1);
                }
            	gtk_tree_path_free(path);
                
            	cur = cur->next;
            }
            
            g_list_free(l);
            str_data = g_strjoinv("\n", entries);
            g_strfreev(entries);
            gtk_clipboard_set_text(mainview->clipboard, str_data, -1);
            g_free(str_data);
            
            str_data = g_strdup_printf(_("Copied %d entries"), selected_rows);
            hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, str_data);
            g_free(str_data);
            
        }
}

/*
 * paste 
 */
void callback_edit_paste(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd->typ == NODE_TEXT)
		gtk_text_buffer_paste_clipboard(GTK_TEXT_BUFFER(mainview->buffer), mainview->clipboard, NULL, TRUE);
	else if (nd->typ == NODE_SKETCH)
		{
		if (sketchwidget_paste(mainview->sk, mainview->clipboard)==FALSE)
							hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error pasting"));
		}
	else if (nd->typ == NODE_CHECKLIST) {
		/* Paste string from clipboard as new item */
		callback_checklist_paste(mainview);
	}

	mainview->file_edited = TRUE;
}

gint cb_popup(GtkWidget * widget, GdkEvent * event)
{
	GtkMenu *menu;
	GdkEventButton *event_button;

	/*
	 * The "widget" is the menu that was supplied when 
	 * * g_signal_connect_swapped() was called.
	 */
	menu = GTK_MENU(widget);
	event_button = (GdkEventButton *) event;
	if (event->type == GDK_BUTTON_PRESS && event_button->button == 3)
		{
			gtk_menu_popup(menu, NULL, NULL, NULL, NULL, event_button->button, event_button->time);
			return TRUE;
		}
	return FALSE;
}

/*
 * close 
 */
gboolean closefile(MainView * mainview)
{
	saveCurrentData(mainview);

	if (mainview->file_edited)
		{
			HildonNote *hn = HILDON_NOTE(hildon_note_new_confirmation_add_buttons(GTK_WINDOW(mainview->data->main_view), _("Save changes?"), _("Yes"), CONFRESP_YES, _("No"), CONFRESP_NO, _("Cancel"), CONFRESP_CANCEL, NULL, NULL));
			gint answer = gtk_dialog_run(GTK_DIALOG(hn));
			gtk_widget_destroy(GTK_WIDGET(hn));

			if (answer == CONFRESP_CANCEL)
				return (FALSE);
			else if (answer == CONFRESP_YES)
				{
					if (mainview->file_name == NULL)
						{
							mainview->file_name = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, "maemopaddata", "db");
						}
					write_buffer_to_file(mainview);
				}
		}

	if (mainview->db)
		sqlite3_close(mainview->db);
	mainview->db = NULL;
	return (TRUE);
}

gboolean callback_file_close(GtkAction * action, gpointer data)
{

	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);
	if (closefile(mainview) == FALSE)
		return(FALSE);

	gtk_main_quit();
	return(TRUE);
}

void callback_file_new_node(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeType typ = NODE_SKETCH;

	nodeData *nd = getSelectedNode(mainview);

	if (nd != NULL)
		typ = nd->typ;

	new_node_dialog(typ, mainview);
}

/*
 * new 
 */
void callback_file_new(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	gchar *filename = NULL;

	if (closefile(mainview) == FALSE)
		return;

	filename = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, "memos", "db");
	if (filename == NULL)
		return;

	new_file(mainview);

	do
		{
		setBusy(mainview, 1);

		int rc;
	
		rc = sqlite3_open(filename, &mainview->db);
		if (rc)
			{
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error 1"));
				fprintf(stderr, "Can't create database %s: %s\n", filename, sqlite3_errmsg(mainview->db));
				break;
			}
			
		sqlite3_exec(mainview->db, "PRAGMA synchronous = OFF;", NULL, NULL, NULL);

		char tq[512];
	
		snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", misctable_name, misctable);
		sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
	
		snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", datatable_name, datatable);
		if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
			{
				fprintf(stderr, "ERROR creating data table\n");
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error 2"));
				break;
			}

		snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", checklisttable_name, checklisttable);
		if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
			{
				fprintf(stderr, "ERROR creating checklist table\n");
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Error 3"));
				break;
			}
	
		if (mainview->db)
			sqlite3_close(mainview->db);
		mainview->db = NULL;
	
		mainview->file_name = filename;
		mainview->file_edited = FALSE;
		read_file_to_buffer(mainview);

/*add a starter memo*/
		nodeData *node;
		node = g_malloc(sizeof(nodeData));
		node->typ = NODE_SKETCH;
		node->name = g_strdup(_("My first memo"));
		node->namepix = NULL;
		node->lastMod = 0;
		node->flags = 0;
		node->sql3id = 0;
		add_new_node(node, mainview, TRUE);
		gtk_paned_set_position(GTK_PANED(mainview->hpaned), 180);
		mainview->viewflags = 3;
		callback_setview(mainview, 1);
		write_buffer_to_file(mainview);

	}while(FALSE);
	setBusy(mainview, 0);
}

gboolean reset_ctree(GtkTreeModel * model, GtkTreePath * path, GtkTreeIter * iter, gpointer data)
{
	nodeData *node;

	gtk_tree_model_get(model, iter, NODE_DATA, &node, -1);
	if (node)
		{
			if (node->name)
				g_free(node->name);
			if (node->namepix)
				g_object_unref(node->namepix);
			g_free(node);

			gtk_tree_store_set(GTK_TREE_STORE(model), iter, NODE_DATA, NULL, -1);
		}
	return (FALSE);
}

void new_file(MainView * mainview)
{
	setBusy(mainview, 1);
	/*
	 * clear buffer, filename and free buffer text 
	 */
	gtk_text_buffer_set_text(GTK_TEXT_BUFFER(mainview->buffer), "", -1);
	mainview->file_name = NULL;
	mainview->file_edited = FALSE;
	mainview->newnodedialog_createchild = TRUE;

	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview));

	g_object_ref(model);
	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), NULL);

	gtk_tree_model_foreach(model, (GtkTreeModelForeachFunc) reset_ctree, (gpointer) mainview);

	/*
	 * crashing bastard
	 * gtk_tree_store_clear(GTK_TREE_STORE(model));
	 */
	GtkTreePath *path = gtk_tree_path_new_from_indices(0, -1);
	GtkTreeIter iter;

	if (gtk_tree_model_get_iter(model, &iter, path))
		{
			do
				{
					gtk_tree_store_remove(GTK_TREE_STORE(model), &iter);
				}
			while(gtk_tree_store_iter_is_valid(GTK_TREE_STORE(model), &iter));
		}
	gtk_tree_path_free(path);

	gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), model);
	g_object_unref(model);

	prepareUIforNodeChange(mainview, NODE_UNKNOWN);
	setBusy(mainview, 2);
}

/*
 * open 
 */
void callback_file_open(GtkAction * action, gpointer data)
{
	gchar *filename = NULL;
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	if (closefile(mainview) == FALSE)
		return;

	/*
	 * open new file 
	 */
	filename = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_OPEN, NULL, NULL);

	/*
	 * if we got a file name from chooser -> open file 
	 */
	open_file(filename, mainview);
	g_free(filename);
}

gboolean open_file(gchar * filename, MainView * mainview)
{
	gboolean ret=FALSE;

	setBusy(mainview, 1);

	while(filename != NULL)
		{
			struct stat s;

			if (stat(filename, &s) == -1) break;

			mainview->file_name = g_strdup(filename);
			gboolean res = read_file_to_buffer(mainview);

			if (res == FALSE)
				{
					g_free(mainview->file_name);
					mainview->file_name = NULL;
					break;
				}
			mainview->file_edited = FALSE;
			ret=TRUE;
			break;
		}

	setBusy(mainview, 2);
	return(ret);
}

void callback_about_link(GtkAboutDialog *about, const gchar *link, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);
	osso_rpc_run_with_defaults(mainview->data->osso, "osso_browser", OSSO_BROWSER_OPEN_NEW_WINDOW_REQ, NULL,
			DBUS_TYPE_STRING, link, DBUS_TYPE_INVALID);
}
                                             
void callback_about(GtkAction * action, gpointer data)
{
	MainView *mainview=(MainView *)data;
	
	const char *authors[] = {
		"Kemal Hadimli",
		"Thomas Perl",
		"Anil Kumar",
		"Michal Seben",
		"Marko Vertainen",
		NULL
	};

	gtk_about_dialog_set_url_hook(callback_about_link, mainview, NULL);
	
	gtk_show_about_dialog(GTK_WINDOW(mainview->data->main_view),
			"name", "Maemopad+",
			"authors", authors,
			"copyright", "(c) 2006-2008 Kemal Hadimli\n(c) 2008-2010 Thomas Perl",
			"license", "GNU LGPL version 2.1 or later\n\nSee http://www.gnu.org/licenses/lgpl.html",
			"version", VERSION,
			"website", "http://maemopadplus.garage.maemo.org/",
			NULL);

}

/*
 * save 
 */
void callback_file_save(GtkAction * action, gpointer data)
{
	gchar *filename = NULL;
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	/*
	 * check is we had a new file 
	 */
	if (mainview->file_name != NULL)
		{
			write_buffer_to_file(mainview);
		}
	else
		{
			filename = interface_file_chooser(mainview, GTK_FILE_CHOOSER_ACTION_SAVE, "maemopaddata", "db");
			/*
			 * if we got a file name from chooser -> save file 
			 */
			if (filename != NULL)
				{
					mainview->file_name = filename;
					write_buffer_to_file(mainview);
					mainview->file_edited = FALSE;
				}
		}
}

void callback_shapemenu(GtkAction * action, GtkWidget * wid)
{
	int style = (int)gtk_object_get_user_data(GTK_OBJECT(wid));
	MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");

	g_assert(mainview != NULL);

	if (style==0) sketchwidget_set_shape(mainview->sk, SKETCHSHAPE_FREEHAND);
	else if (style==1) sketchwidget_set_shape(mainview->sk, SKETCHSHAPE_LINE);
	else if (style==2) sketchwidget_set_shape(mainview->sk, SKETCHSHAPE_RECT);
	else if (style==3) sketchwidget_set_shape(mainview->sk, SKETCHSHAPE_ELLIPSE);
}



void callback_eraser(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb)) == TRUE)
		{
			GdkColor c2;

			c2.red = 65535;
			c2.green = 65535;
			c2.blue = 65535;

			mainview->sk->pressuresensitivity=FALSE;

			sketchwidget_set_brushcolor(mainview->sk, c2);
			mainview->brushsize_backup = sketchwidget_get_brushsize(mainview->sk);
			guint ers=(mainview->brushsize_backup*4)+4;
			sk_set_brushsize(mainview, ers);
			sketchwidget_set_brushsize(mainview->sk, ers); /*fixme:to override max brush size, not pretty*/
		}
	else
		{
			GdkColor c2;
      
			  hildon_color_button_get_color(HILDON_COLOR_BUTTON(mainview->colorbutton), &c2);

			mainview->sk->pressuresensitivity=gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(mainview->tools_pressure));

			sketchwidget_set_brushcolor(mainview->sk, c2);
			sk_set_brushsize(mainview, mainview->brushsize_backup);
		}
}

void callback_toggletree(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[0]), gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mainview->toggletree_tb)));

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mainview->toggletree_tb))==TRUE)
		mainview->viewflags |= 1;
	else
		mainview->viewflags &= ~1;
		
	callback_setview(mainview, 0);
}

void callback_menu(GtkAction * action, GtkWidget * menu)
{
	gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 0, GDK_CURRENT_TIME);
}

void callback_brushsizetb(GtkAction * action, MainView *mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb)) == TRUE)
		{
			gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb), FALSE);
		}
	else
		{
			callback_menu(NULL, mainview->brushsizemenu);
		}
}

void callback_brushsize(GtkAction * action, GtkWidget * wid)
{
	int bsize = (int)gtk_object_get_user_data(GTK_OBJECT(wid));
	MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");

	g_assert(mainview != NULL && mainview->data != NULL);

	sketchwidget_set_brushsize(mainview->sk, bsize);

	GtkWidget *pix = gtk_object_get_data(GTK_OBJECT(wid), "i");

	gtk_widget_show(pix);
	gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(mainview->brushsize_tb), pix);
}

void callback_sketchlines(GtkAction * action, GtkWidget * wid)
{
	int style = (int)gtk_object_get_user_data(GTK_OBJECT(wid));
	MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");

	g_assert(mainview != NULL);

	nodeData *nd = getSelectedNode(mainview);
	gboolean doit = FALSE;

	if (nd != NULL && nd->typ == NODE_SKETCH)
		{
			nd->flags &= ~NODEFLAG_SKETCHLINES;
			nd->flags &= ~NODEFLAG_SKETCHGRAPH;
/*			sketchwidget_set_edited(mainview->sk, TRUE);*/ /*we call this on openfile, so this messes things up*/
			doit = TRUE;
		}

	if (style == 0)
		{
			sketchwidget_set_backstyle(mainview->sk, SKETCHBACK_NONE);
		}
	else if (style == 1)
		{
			sketchwidget_set_backstyle(mainview->sk, SKETCHBACK_LINES);
			if (doit == TRUE)
				nd->flags |= NODEFLAG_SKETCHLINES;
		}
	else if (style == 2)
		{
			sketchwidget_set_backstyle(mainview->sk, SKETCHBACK_GRAPH);
			if (doit == TRUE)
				nd->flags |= NODEFLAG_SKETCHGRAPH;
		}

	GtkWidget *pix = gtk_object_get_data(GTK_OBJECT(wid), "i");

	gtk_widget_show(pix);
	gtk_tool_button_set_icon_widget(GTK_TOOL_BUTTON(mainview->sketchlines_tb), pix);
}

void callback_color(HildonColorButton * colorButton, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);
	if (nd == NULL) return;

	GdkColor c2;
	hildon_color_button_get_color(colorButton, &c2);

	if (nd->typ == NODE_SKETCH)
		{
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb), FALSE);
		sketchwidget_set_brushcolor(mainview->sk, c2);
		}
	else if (nd->typ == NODE_CHECKLIST)
		{
		GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
		GList* l=gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)), NULL);
	
		char tmp[10];
		snprintf(tmp, sizeof(tmp), "#%02x%02x%02x", c2.red>>8, c2.green>>8, c2.blue>>8);

	 	GList* cur=l;
	 	while(cur)
	 		{
	 		GtkTreePath *path=cur->data;
	
			GtkTreeIter iter;
			if (gtk_tree_model_get_iter(model, &iter, path))
				{
			  gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_COLOR, tmp, -1);
				}
	 		gtk_tree_path_free(path);
	 		cur=cur->next;
	 		}
		g_list_free(l);
		}
}

void callback_color_invoke(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	gtk_button_clicked(GTK_BUTTON(mainview->colorbutton));
}



void callback_pressure(GtkAction * action, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

nodeData *nd = getSelectedNode(mainview);

if (nd == NULL)
	return;
if (nd->typ != NODE_SKETCH)
	return;
	
mainview->sk->pressuresensitivity=gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(mainview->tools_pressure));
}


void callback_wordwrap(GtkAction * action, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

nodeData *nd = getSelectedNode(mainview);

if (nd == NULL)
	return;
if (nd->typ != NODE_TEXT)
	return;
	
gboolean act=gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(mainview->tools_wordwrap));
if (act==TRUE) nd->flags |= NODEFLAG_WORDWRAP;
		else nd->flags &= ~NODEFLAG_WORDWRAP;

gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(mainview->textview), (act==TRUE)?GTK_WRAP_WORD:GTK_WRAP_NONE);
}


void callback_font(GtkAction * action, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd == NULL)
		return;
	if (nd->typ != NODE_TEXT)
		return;

HildonFontSelectionDialog *dialog = HILDON_FONT_SELECTION_DIALOG(hildon_font_selection_dialog_new(NULL, NULL));

gboolean gotsel=wp_text_buffer_has_selection(mainview->buffer);
/*gotsel=FALSE;*/

WPTextBufferFormat fmt;
wp_text_buffer_get_attributes(mainview->buffer, &fmt, gotsel);

gint ri=0;
if (fmt.text_position==TEXT_POSITION_SUPERSCRIPT) ri=1;
else if (fmt.text_position==TEXT_POSITION_SUBSCRIPT) ri=-1;

g_object_set(G_OBJECT(dialog),
	"family-set", fmt.cs.font,
	"family", wp_get_font_name(fmt.font),
	"size-set", fmt.cs.font_size,
	"size", wp_font_size[fmt.font_size],
	"color-set", fmt.cs.color,
	"color", &fmt.color,
	"bold-set", fmt.cs.bold,
	"bold", fmt.bold,
	"italic-set", fmt.cs.italic,
	"italic", fmt.italic,
	"underline-set", fmt.cs.underline,
	"underline", fmt.underline,
	"strikethrough-set", fmt.cs.strikethrough,
	"strikethrough", fmt.strikethrough,
	"position-set", fmt.cs.text_position,
	"position", ri,
	NULL);

gtk_widget_show_all(GTK_WIDGET(dialog));
if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_OK)
	{
	gboolean bold, italic, underline, strikethrough;
	gchar *family = NULL;
	gint size, position;
	GdkColor *color=NULL;
	gboolean set_family, set_size, set_bold, set_italic, set_underline, set_strikethrough, set_color, set_position;

	g_object_get(G_OBJECT(dialog), "family", &family, "size", &size, "bold", &bold, "italic", &italic,
	"underline", &underline, "strikethrough", &strikethrough,
	"family-set", &set_family, "size-set", &set_size, "bold-set", &set_bold, "italic-set", &set_italic,
	"underline-set", &set_underline, "strikethrough-set", &set_strikethrough,
	"color", &color, "color-set", &set_color, "position", &position, "position-set", &set_position,
	NULL);

	wp_text_buffer_get_attributes(mainview->buffer, &fmt, FALSE);
	fmt.cs.font=fmt.cs.font_size=fmt.cs.strikethrough=fmt.cs.color=fmt.cs.bold=fmt.cs.italic=fmt.cs.underline=fmt.cs.text_position=0;

	if (set_family) { fmt.font=wp_get_font_index(family, 1); fmt.cs.font=1; }
	if (set_size) { fmt.font_size=wp_get_font_size_index(size, 16); fmt.cs.font_size=1; }

	if (set_strikethrough)
		{
		fmt.cs.strikethrough=1;
		fmt.strikethrough=strikethrough;
		}

	if (set_color)
		{
/*
GLIB WARNING ** GLib-GObject - IA__g_object_set_valist: object class `GtkTextTag' has no property named `'
*/
		fmt.cs.color=1;
		fmt.color.pixel=color->pixel;
		fmt.color.red=color->red;
		fmt.color.green=color->green;
		fmt.color.blue=color->blue;
		}

	if (set_position)
		{
		if (position==1) ri=TEXT_POSITION_SUPERSCRIPT;
		else if (position==-1) ri=TEXT_POSITION_SUBSCRIPT;
		else ri=TEXT_POSITION_NORMAL;

		fmt.cs.text_position=1;
		fmt.text_position=ri;
		}

	if (set_bold)
		{
		fmt.cs.bold=1;
		fmt.bold=bold;
		}
	if (set_italic)
		{
		fmt.cs.italic=1;
		fmt.italic=italic;
		}
	if (set_underline)
		{
		fmt.cs.underline=1;
		fmt.underline=underline;
		}

	wp_text_buffer_set_format(mainview->buffer, &fmt);
	}
		
gtk_widget_destroy(GTK_WIDGET(dialog));
}

void callback_fontstyle(GtkAction * action, GtkWidget * wid)
{
MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");
g_assert(mainview != NULL && mainview->data != NULL);

nodeData *nd = getSelectedNode(mainview);

if (nd == NULL)
	return;
if (nd->typ == NODE_TEXT)
	{
	gboolean act=gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(wid));
	
	gint style = (gint)gtk_object_get_data(GTK_OBJECT(wid), "s");
	wp_text_buffer_set_attribute(mainview->buffer, style, (gpointer)act);
	}
else if (nd->typ == NODE_CHECKLIST)
	{
	gboolean act=gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(wid));
	gint style = (gint)gtk_object_get_data(GTK_OBJECT(wid), "s");
	if (style!=WPT_BOLD && style!=WPT_STRIKE && style!=WPT_LEFT) return;

	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
	GList* l=gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)), NULL);

	gint styletoset_weight=PANGO_WEIGHT_NORMAL;
	gboolean styletoset_strike=FALSE;
	gboolean checkit=FALSE;

	if (style==WPT_BOLD && act==TRUE) styletoset_weight=PANGO_WEIGHT_BOLD;
	else if (style==WPT_STRIKE && act==TRUE) styletoset_strike=TRUE;
	else if (style==WPT_LEFT && act==TRUE) checkit=TRUE;

 	GList* cur=l;
 	while(cur)
 		{
 		GtkTreePath *path=cur->data;

		GtkTreeIter iter;
		if (gtk_tree_model_get_iter(model, &iter, path))
			{
		  if (style==WPT_BOLD) gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_BOLD, styletoset_weight, -1);
		  else if (style==WPT_STRIKE) gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_STRIKE, styletoset_strike, -1);
		  else if (style==WPT_LEFT) gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_CHECKED, checkit, -1);
			}
 		gtk_tree_path_free(path);
 		cur=cur->next;
 		}

	g_list_free(l);
	gtk_object_set_data(GTK_OBJECT(mainview->listview), "edited", TRUE);
	}
}


void callback_textbuffer_move(WPTextBuffer *textbuffer, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

/*
gboolean gotsel=wp_text_buffer_has_selection(mainview->buffer);

_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), gotsel);
_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->italic_tb), gotsel);
_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->underline_tb), gotsel);
_toggle_tool_button_set_inconsistent(GTK_TOGGLE_TOOL_BUTTON(mainview->bullet_tb), gotsel);
*/
WPTextBufferFormat fmt;
wp_text_buffer_get_attributes(mainview->buffer, &fmt, FALSE/*gotsel*/);

g_signal_handlers_block_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_block_by_func(mainview->italic_tb, callback_fontstyle, mainview->italic_tb);
g_signal_handlers_block_by_func(mainview->underline_tb, callback_fontstyle, mainview->underline_tb);
g_signal_handlers_block_by_func(mainview->bullet_tb, callback_fontstyle, mainview->bullet_tb);

gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), fmt.bold);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->italic_tb), fmt.italic);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->underline_tb), fmt.underline);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bullet_tb), fmt.bullet);

g_signal_handlers_unblock_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_unblock_by_func(mainview->italic_tb, callback_fontstyle, mainview->italic_tb);
g_signal_handlers_unblock_by_func(mainview->underline_tb, callback_fontstyle, mainview->underline_tb);
g_signal_handlers_unblock_by_func(mainview->bullet_tb, callback_fontstyle, mainview->bullet_tb);
}

gint wp_savecallback(const gchar *buffer, GString * gstr)
{
gstr=g_string_append(gstr, buffer);
return(0);
}

void callback_undo(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd == NULL) return;
	
	if (nd->typ == NODE_SKETCH) sketchwidget_undo(mainview->sk);
	else if (nd->typ == NODE_TEXT) wp_text_buffer_undo(mainview->buffer);
}

void callback_redo(GtkAction * action, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	nodeData *nd = getSelectedNode(mainview);

	if (nd == NULL) return;
	
	if (nd->typ == NODE_SKETCH) sketchwidget_redo(mainview->sk);
	else if (nd->typ == NODE_TEXT) wp_text_buffer_redo(mainview->buffer);
}

void callback_undotoggle(gpointer widget, gboolean st, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	gtk_widget_set_sensitive(GTK_WIDGET(mainview->undo_tb), st);
}

void callback_redotoggle(gpointer widget, gboolean st, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	gtk_widget_set_sensitive(GTK_WIDGET(mainview->redo_tb), st);
}

void callback_finger(SketchWidget * sk, gint x, gint y, gdouble pressure, MainView * mainview)
{
	g_assert(mainview != NULL && mainview->data != NULL);

	if ((mainview->viewflags & 2) == 0) mainview->viewflags |= 2;
																	else mainview->viewflags &= ~2;
	callback_setview(mainview, 1);
}

gboolean close_cb(GtkWidget * widget, GdkEventAny * event, MainView * mainview)
{
	callback_file_close(NULL, mainview);
	return (TRUE);
}

gboolean key_press_cb(GtkWidget * widget, GdkEventKey * event, MainView * mainview)
{
	switch (event->keyval)
		{

			/*
			 * case GDK_Up:
			 * gtk_infoprint(GTK_WINDOW(app), "Navigation Key Up");
			 * return TRUE;
			 * 
			 * case GDK_Down:
			 * gtk_infoprint(GTK_WINDOW(app), "Navigation Key Down");
			 * return TRUE;
			 * 
			 * case GDK_Left:
			 * gtk_infoprint(GTK_WINDOW(app), "Navigation Key Left");
			 * return TRUE;
			 * 
			 * case GDK_Right:
			 * gtk_infoprint(GTK_WINDOW(app), "Navigation Key Right");
			 * return TRUE;
			 * 
			 * case GDK_Return:
			 * gtk_infoprint(GTK_WINDOW(app), "Navigation Key select");
			 * return TRUE;
			 */
/*code below messes up when you have a textview*/
/*
		case GDK_Left:
		case GDK_Right:
			{
			gtk_widget_child_focus(widget, event->keyval==GDK_Left?GTK_DIR_TAB_BACKWARD:GTK_DIR_TAB_FORWARD);
			return TRUE;
			}
*/
		case GDK_Left:
			{
			nodeData *selnode = getSelectedNode(mainview);
			if (selnode!=NULL && selnode->typ==NODE_SKETCH)
				{
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Squared shapes ON"));
				sketchwidget_set_shift(mainview->sk, TRUE);
				return TRUE;
				}
			return FALSE;
			}
		case GDK_Right:
			{
			nodeData *selnode = getSelectedNode(mainview);
			if (selnode!=NULL && selnode->typ==NODE_SKETCH)
				{
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Squared shapes OFF"));
				sketchwidget_set_shift(mainview->sk, FALSE);
				return TRUE;
				}
			return FALSE;
			}
		case GDK_Down:
			{
			nodeData *selnode = getSelectedNode(mainview);
			if (selnode!=NULL && selnode->typ==NODE_SKETCH)
				{
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Filled shapes OFF"));
				sketchwidget_set_fillmode(mainview->sk, FALSE);
				return TRUE;
				}
			return FALSE;
			}
		case GDK_Up:
			{
			nodeData *selnode = getSelectedNode(mainview);
			if (selnode!=NULL && selnode->typ==NODE_SKETCH)
				{
				hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Filled shapes ON"));
				sketchwidget_set_fillmode(mainview->sk, TRUE);
				return TRUE;
				}
			return FALSE;
			}
		case GDK_F6:
			{
			if ((mainview->viewflags & 4) == 0) mainview->viewflags |= 4;
																			else mainview->viewflags &= ~4;
			callback_setview(mainview, 1);
			return TRUE;
			}
		case GDK_F7:
			{
			callback_redo(NULL, mainview);
			return TRUE;
			}
		case GDK_F8:
			{
			callback_undo(NULL, mainview);
			return TRUE;
			}
		case GDK_Escape:
				{
				if ((mainview->viewflags & 1) == 0) mainview->viewflags |= 1;
																				else mainview->viewflags &= ~1;
				callback_setview(mainview, 1);

				return TRUE;
				}
/*
		case GDK_Return:
				{
				if ((mainview->viewflags & 2) == 0) mainview->viewflags |= 2;
																				else mainview->viewflags &= ~2;
				callback_setview(mainview, 1);
				return TRUE;
				}
*/
		}

	return FALSE;
}

void callback_viewmenu(GtkAction * action, GtkWidget * wid)
{
	int flag = (int)gtk_object_get_user_data(GTK_OBJECT(wid));
	MainView *mainview = gtk_object_get_data(GTK_OBJECT(wid), "m");

	g_assert(mainview != NULL);

	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(wid))) mainview->viewflags |= flag;
	 																												 else mainview->viewflags &= ~flag;
	 																												 
	if (flag==1)
		{
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->toggletree_tb), gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(wid)));
		return;			
		}

	callback_setview(mainview, 0);
}


void callback_setview(MainView *mainview, int setmenu)
{
if (setmenu>0)
	{
	if ((mainview->viewflags&4)>0)
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[2]), TRUE);
	else
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[2]), FALSE);

	if ((mainview->viewflags&2)>0)
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[1]), TRUE);
	else
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[1]), FALSE);
		
	if ((mainview->viewflags&1)>0)
		{
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[0]), TRUE);
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->toggletree_tb), TRUE);
		}
	else
		{
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(mainview->viewmenuitems[0]), FALSE);
		gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->toggletree_tb), FALSE);
		}
	
	return;
	}

	setBusy(mainview, 1);

	if ((mainview->viewflags&4)>0)
		gtk_window_fullscreen(GTK_WINDOW(mainview->data->main_view));	
	else
		gtk_window_unfullscreen(GTK_WINDOW(mainview->data->main_view));

	if ((mainview->viewflags&2)>0)
		gtk_widget_show(mainview->toolbar);
	else
		gtk_widget_hide(mainview->toolbar);

	if ((mainview->viewflags&1)>0)
		gtk_widget_show(mainview->scrolledtree);
	else
		gtk_widget_hide(mainview->scrolledtree);

	if (mainview->viewflags==4)
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sketchwidget_get_mainwidget(mainview->sk)), GTK_POLICY_NEVER, GTK_POLICY_NEVER);
	else
		gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sketchwidget_get_mainwidget(mainview->sk)), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	setBusy(mainview, 2);
}


void callback_buffer_modified(GtkAction * action, gpointer data)
{
MainView *mainview = (MainView *) data;
g_assert(mainview != NULL && mainview->data != NULL);

mainview->file_edited = TRUE;
}

/*
 * Callback for exit D-BUS event 
 */
void exit_event_handler(gboolean die_now, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	if (!die_now)
		{
		if (callback_file_close(NULL, mainview)==FALSE) gtk_main_quit(); /*make sure we call gtk_main_quit*/
		}
	else
		{
		gtk_main_quit();
		}	
	/*
	 * application_exit(mainview->data);
	 */
}

/*
 * Callback for hardware D-BUS events 
 */
void hw_event_handler(osso_hw_state_t * state, gpointer data)
{
	MainView *mainview = (MainView *) data;
	g_assert(mainview != NULL && mainview->data != NULL);

	/*
	 * if (state->shutdown_ind)
	 * {
	 * gtk_infoprint(GTK_WINDOW(mainview->data->app),
	 * "Shutdown event!");
	 * }
	 * if (state->memory_low_ind)
	 * {
	 * gtk_infoprint(GTK_WINDOW(mainview->data->app),
	 * "Memory low event!");
	 * }
	 */
	if (state->save_unsaved_data_ind)
		{
			fprintf(stderr, "Saving unsaved data!\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Saving unsaved data!");
			callback_file_save(NULL, mainview);
		}

	/*
	 * if (state->system_inactivity_ind)
	 * {
	 * gtk_infoprint(GTK_WINDOW(mainview->data->app),
	 * "Minimize application inactivity event!");
	 * }
	 */
}

GtkTreeRowReference *read_sqlite3_data(MainView * mainview, unsigned int parentid, GtkTreeRowReference * parenttree, unsigned int selected, GtkTreeStore * model)
{
	GtkTreeRowReference *resref = NULL;

	char q[256];

	snprintf(q, sizeof(q), "SELECT nodeid, bodytype, name, nameblob, lastmodified, flags FROM %s WHERE parent=%d ORDER BY ord", datatable_tmpname, parentid);

	sqlite3_stmt *stmt = NULL;
	const char *dum;
	int rc = sqlite3_prepare(mainview->db, q, strlen(q), &stmt, &dum);

	if (rc)
		{
			fprintf(stderr, "Error %s\n", sqlite3_errmsg(mainview->db));
			return (NULL);
		}

	rc = SQLITE_BUSY;
	while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
		{
			rc = sqlite3_step(stmt);
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
				break;
			else if (rc == SQLITE_ROW)
				{
					int nodeid = sqlite3_column_int(stmt, 0);
					int typ = sqlite3_column_int(stmt, 1);
					const unsigned char *name = sqlite3_column_text(stmt, 2);
					const unsigned char *nameblob = sqlite3_column_text(stmt, 3);
					int lastmod = sqlite3_column_int(stmt, 4);
					int flags = sqlite3_column_int(stmt, 5);

					/*
					 * fprintf(stderr, "CARD=%s TYPE=%d\n", name, typ);
					 */
					if ((typ != NODE_TEXT && typ != NODE_SKETCH && typ != NODE_CHECKLIST) || (name == NULL && nameblob == NULL))
						{

							/*
							 * fprintf(stderr, "invalid card, skipping\n");
							 */
							continue;
						}

					nodeData *node = g_malloc(sizeof(nodeData));

					node->sql3id = nodeid;
					node->typ = typ;
					node->flags = flags;
					node->name = NULL;
					node->namepix = NULL;
					if (name != NULL)
						node->name = g_strdup((char *)name);
					if (nameblob != NULL)
						{
							int blobsize = sqlite3_column_bytes(stmt, 3);

							GdkPixbufLoader *pl = gdk_pixbuf_loader_new_with_type("png", NULL);
							GError *err = NULL;

							gdk_pixbuf_loader_write(pl, (guchar *) nameblob, blobsize, &err);
							if (err != NULL)
								{
									fprintf(stderr, "Error loading nodename! %s\n", err->message);
									g_error_free(err);
									err = NULL;
								}
							gdk_pixbuf_loader_close(pl, NULL);
							GdkPixbuf *pixbuf = gdk_pixbuf_loader_get_pixbuf(pl);

							if (GDK_IS_PIXBUF(pixbuf))
								node->namepix = pixbuf;
						}
					node->lastMod = lastmod;

					GtkTreeIter parentiter, newiter;
					void *par = NULL;

					if (parenttree != NULL)
						{
							GtkTreePath *pa = gtk_tree_row_reference_get_path(parenttree);

							gtk_tree_model_get_iter(GTK_TREE_MODEL(model), &parentiter, pa);
							gtk_tree_path_free(pa);
							par = &parentiter;
						}

					gtk_tree_store_append(model, &newiter, par);
					gtk_tree_store_set(model, &newiter, NODE_NAME, node->name, NODE_PIXBUF, node->namepix, NODE_DATA, node, -1);

					GtkTreePath *pa = gtk_tree_model_get_path(GTK_TREE_MODEL(model), &newiter);

					GtkTreeRowReference *newref = gtk_tree_row_reference_new(GTK_TREE_MODEL(model), pa);

					if (selected == nodeid)
						resref = newref;

					gtk_tree_path_free(pa);
					GtkTreeRowReference *r = read_sqlite3_data(mainview, nodeid, newref, selected,
																										 model);

					if (resref != newref)
						gtk_tree_row_reference_free(newref);

					if (r != NULL)
						{
							if (resref == NULL)
								resref = r;
							else
								gtk_tree_row_reference_free(r);	/*safeguard */
						}
				}
		}

	if (stmt)
		sqlite3_finalize(stmt);

	return (resref);							/*ref to supposed-to-be-selected treeitem */
}

/*
 * read file 
 */
gboolean read_file_to_buffer(MainView * mainview)
{
	char tq[512];

	g_assert(mainview != NULL);
	gboolean res = FALSE;

	gchar *filename = mainview->file_name;

	new_file(mainview);
	mainview->file_name = filename;
	mainview->loading=TRUE;

	fprintf(stderr, "read:*%s*\n", filename);

	int rc;
	sqlite3_stmt *stmt = NULL;

	rc = sqlite3_open(filename, &mainview->db);
	do
		{
			if (rc)
				{
					fprintf(stderr, "Can't open database %s: %s\n", filename, sqlite3_errmsg(mainview->db));
					break;
				}
				
			sqlite3_exec(mainview->db, "PRAGMA synchronous = OFF;", NULL, NULL, NULL);

			char *q = "SELECT skey, sval FROM settings";
			const char *dum;

			rc = sqlite3_prepare(mainview->db, q, strlen(q), &stmt, &dum);
			if (rc)
				{
					fprintf(stderr, "Error %s\n", sqlite3_errmsg(mainview->db));
					break;
				}

			unsigned int selectedCard = 0;

			/*
			 * fprintf(stderr, "start config\n");
			 */
			unsigned int curDataVersion = 0;
			unsigned int curChecklistVersion = 0;

			rc = SQLITE_BUSY;
			while(rc == SQLITE_BUSY || rc == SQLITE_ROW)
				{
					rc = sqlite3_step(stmt);
					if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE || rc == SQLITE_DONE)
						break;
					else if (rc == SQLITE_ROW)
						{
							const unsigned char *col_key = sqlite3_column_text(stmt, 0);
							const unsigned char *col_val = sqlite3_column_text(stmt, 1);

							/*
							 * fprintf(stderr, "%s=%s\n", col_key, col_val);
							 */
							if (!strcmp(col_key, "leftPanedPos"))
								{
									gint panedPos = atoi((char *)col_val);

									if (panedPos > 0)
										gtk_paned_set_position(GTK_PANED(mainview->hpaned), panedPos);
								}
							if (!strcmp(col_key, "selectedNode"))
								{
									gint tmp = atoi((char *)col_val);

									if (tmp > 0)
										selectedCard = tmp;
								}
							if (!strcmp(col_key, "dataVersion"))
								{
									gint tmp = atoi((char *)col_val);

									if (tmp > 0)
										curDataVersion = tmp;
								}
							if (!strcmp(col_key, "checklistVersion"))
								{
									gint tmp = atoi((char *)col_val);

									if (tmp > 0)
										curChecklistVersion = tmp;
								}
							if (!strcmp(col_key, "newNodeDlgCreateChild"))
								{
									gint tmp = atoi((char *)col_val);

									mainview->newnodedialog_createchild = TRUE;
									if (tmp == 0)
										mainview->newnodedialog_createchild = FALSE;
								}
							if (!strcmp(col_key, "fullScreen"))
								{
									gint tmp = atoi((char *)col_val);
									if (tmp<0 || tmp>4) tmp=0;
									if (tmp==0) tmp=4;
												 else tmp--;
									if (tmp==0) tmp=3;
									else if (tmp==1) tmp=2;
									else if (tmp==2) tmp=7;
									else if (tmp==3) tmp=6;
									else if (tmp==4) tmp=4;
									mainview->viewflags=tmp;
									callback_setview(mainview, TRUE);
								}
							if (!strcmp(col_key, "viewFlags"))
								{
									gint tmp = atoi((char *)col_val);
									if (tmp<0) tmp=3;
									mainview->viewflags=tmp;
									callback_setview(mainview, TRUE);
								}
							if (!strcmp(col_key, "brushSize"))
								{
									gint tmp = atoi((char *)col_val);
									if (tmp>0) sk_set_brushsize(mainview, tmp);
								}
							if (!strcmp(col_key, "brushColor"))
								{
									unsigned long tmp = atol((char *)col_val);
									GdkColor c2;
						
									c2.red = ((tmp & 0xFF0000) >> 16) << 8;
									c2.green = ((tmp & 0xFF00) >> 8) << 8;
									c2.blue = (tmp & 0xFF) << 8;
/*	fprintf(stderr, "READ BRUSHCOLOR is %ul (%d,%d,%d)\n", tmp, c2.red, c2.green, c2.blue);*/
				
									sketchwidget_set_brushcolor(mainview->sk, c2);
									hildon_color_button_set_color(HILDON_COLOR_BUTTON(mainview->colorbutton), &c2);
								}

						}
				}
			if (rc == SQLITE_ERROR || rc == SQLITE_MISUSE)
				{
					fprintf(stderr, "Error2 %s\n", sqlite3_errmsg(mainview->db));
					break;
				}

			/*
			 * fprintf(stderr, "end config\n");
			 */
			if (stmt)
				sqlite3_finalize(stmt);

			gboolean resback = FALSE;

			while(curDataVersion < datatableversion)
				{
					if (curDataVersion == 0)
						{
							snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

							snprintf(tq, sizeof(tq), "ALTER TABLE %s RENAME TO %s", datatable_name, datatable_backupname);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
								{
									fprintf(stderr, "ERROR backing up table!\n");
									break;
								}
							resback = TRUE;

							snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", datatable_name, datatable);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
								{
									fprintf(stderr, "ERROR creating table!\n");
									break;
								}
							snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT nodeid, parent, bodytype, name, body, nameblob, bodyblob, lastmodified, ord, 0 FROM %s", datatable_name, datatable_backupname);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
								{
									fprintf(stderr, "ERROR copying data!\n");
									break;
								}

							snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

							curDataVersion = datatableversion;
						}
				break;
				}

			if (curDataVersion != datatableversion)
				{
					fprintf(stderr, "Data version mismatch\n");

					if (resback == TRUE)
						{
							snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_name);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
							snprintf(tq, sizeof(tq), "ALTER TABLE %s RENAME TO %s", datatable_backupname, datatable_name);
							sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
						}

					break;
				}


			while(curChecklistVersion < checklisttableversion)
				{
					if (curChecklistVersion == 0) /*no checklisttable at all*/
						{
							snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", checklisttable_name, checklisttable);
							if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
								{
									fprintf(stderr, "ERROR creating checklist table!\n");
									break;
								}
							curChecklistVersion = checklisttableversion;
						}
				break;
				}


			GtkTreeStore *model = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview)));

			g_object_ref(model);
			gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), NULL);

			do
				{
					char tq[512];

					snprintf(tq, sizeof(tq), "CREATE%s TABLE %s%s", TEMPTABLE_KEYWORD, datatable_tmpname, datatable);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						{
							fprintf(stderr, "ERROR creating temp table!\n");
							break;
						}
					snprintf(tq, sizeof(tq), "CREATE INDEX %s_index ON %s %s", datatable_tmpname, datatable_tmpname, dataindex);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						{
							fprintf(stderr, "ERROR creating temp index!\n");
							break;
						}
					snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_tmpname, datatable_name);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						{
							fprintf(stderr, "ERROR copying data to temp table!\n");
							break;
						}

					snprintf(tq, sizeof(tq), "CREATE%s TABLE %s%s", TEMPTABLE_KEYWORD, checklisttable_tmpname, checklisttable);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						{
							fprintf(stderr, "ERROR creating temp table! (checklist)\n");
							break;
						}
					snprintf(tq, sizeof(tq), "CREATE INDEX %s_index ON %s %s", checklisttable_tmpname, checklisttable_tmpname, checklistindex);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						{
							fprintf(stderr, "ERROR creating temp index! (checklist)\n");
							break;
						}
					snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_tmpname, checklisttable_name);
					if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
						{
							fprintf(stderr, "ERROR copying data to temp table! (checklist)\n");
							break;
						}
				}
			while(FALSE);

			GtkTreeRowReference *selectedRef = read_sqlite3_data(mainview, 0, NULL, selectedCard, model);

			gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->treeview), GTK_TREE_MODEL(model));
			g_object_unref(model);
			gtk_tree_view_expand_all(GTK_TREE_VIEW(mainview->treeview));
			
			if (selectedRef != NULL)
				{
					GtkTreeIter seliter;

					if (ref2iter(GTK_TREE_MODEL(model), selectedRef, &seliter) == TRUE)
						{
							GtkTreeSelection *selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->treeview));
							gtk_tree_selection_select_iter(selection, &seliter);
						}

					gtk_tree_row_reference_free(selectedRef);
				}
			res = TRUE;
		}
	while(FALSE);

	if (stmt)
		sqlite3_finalize(stmt);


	mainview->loading=FALSE;
	
	return (res);
}

/*
 * write to file 
 */
void write_buffer_to_file(MainView * mainview)
{
	fprintf(stderr, "write:*%s*\n", mainview->file_name);
	saveCurrentData(mainview);

	GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->treeview));
	/*update ord value in database for all nodes*/
	gtk_tree_model_foreach(GTK_TREE_MODEL(model),(GtkTreeModelForeachFunc) foreach_func_update_ord,mainview);

	setBusy(mainview, 1);
	
	char tq[512];

	snprintf(tq, sizeof(tq), "DROP TABLE %s", misctable_name);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", misctable_name, misctable);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	gint panedPos = gtk_paned_get_position(GTK_PANED(mainview->hpaned));

	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('leftPanedPos', '%d');", misctable_name, panedPos);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	gint nndcc = 1;

	if (mainview->newnodedialog_createchild == FALSE)
		nndcc = 0;
	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('newNodeDlgCreateChild', '%d');", misctable_name, nndcc);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('viewFlags', '%d');", misctable_name, mainview->viewflags);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	nodeData *node = getSelectedNode(mainview);

	if (node)
		{
			snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('selectedNode', '%d');", misctable_name, node->sql3id);
			sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
		}

	guint bsize;
	if (gtk_toggle_tool_button_get_active(GTK_TOGGLE_TOOL_BUTTON(mainview->eraser_tb)) == TRUE)
		{
			bsize=mainview->brushsize_backup;
		}
	else
		{
			bsize=sketchwidget_get_brushsize(mainview->sk);
		}
	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('brushSize', '%d');", misctable_name, bsize);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

  GdkColor col;
  hildon_color_button_get_color(HILDON_COLOR_BUTTON(mainview->colorbutton), &col);
	unsigned long bcol=((col.red >> 8) << 16) | ((col.green >> 8) << 8) | (col.blue >> 8);

/*	fprintf(stderr, "BRUSHCOLOR is %d (%d,%d,%d)\n", bcol, col->red, col->green, col->blue);*/
	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('brushColor', '%lu');", misctable_name, bcol);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('dataVersion', '%d');", misctable_name, datatableversion);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "INSERT INTO %s VALUES('checklistVersion', '%d');", misctable_name, checklisttableversion);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
	snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", datatable_backupname, datatable);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			fprintf(stderr, "ERROR creating backup table!\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error 1");

			setBusy(mainview, 2);
			return;
		}
	snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_backupname, datatable_name);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			fprintf(stderr, "ERROR backing up table!\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error 2");

			setBusy(mainview, 2);
			return;
		}
	snprintf(tq, sizeof(tq), "DELETE FROM %s", datatable_name);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_name, datatable_tmpname);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			fprintf(stderr, "ERROR saving table!\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error 3");

			snprintf(tq, sizeof(tq), "DELETE FROM %s", datatable_name);
			sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

			snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", datatable_name, datatable_backupname);
			if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
				{
					fprintf(stderr, "ERROR restoring backup! data lost!\n");
				}

			setBusy(mainview, 2);
			return;
		}

	snprintf(tq, sizeof(tq), "DROP TABLE %s", datatable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

/*checklist*/
	snprintf(tq, sizeof(tq), "DROP TABLE %s", checklisttable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);
	snprintf(tq, sizeof(tq), "CREATE TABLE %s%s", checklisttable_backupname, checklisttable);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			fprintf(stderr, "ERROR creating backup table! (checklist)\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error 4");

			setBusy(mainview, 2);
			return;
		}

	snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_backupname, checklisttable_name);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			fprintf(stderr, "ERROR backing up table! (checklist)\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error 5");

			setBusy(mainview, 2);
			return;
		}
	snprintf(tq, sizeof(tq), "DELETE FROM %s", checklisttable_name);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_name, checklisttable_tmpname);
	if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
		{
			fprintf(stderr, "ERROR saving table! (checklist)\n");
			hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, "Error 6");

			snprintf(tq, sizeof(tq), "DELETE FROM %s", checklisttable_name);
			sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

			snprintf(tq, sizeof(tq), "INSERT INTO %s SELECT * FROM %s", checklisttable_name, checklisttable_backupname);
			if (sqlite3_exec(mainview->db, tq, NULL, NULL, NULL) != 0)
				{
					fprintf(stderr, "ERROR restoring backup! data lost! (checklist)\n");
				}
			setBusy(mainview, 2);
			return;
		}

	snprintf(tq, sizeof(tq), "DROP TABLE %s", checklisttable_backupname);
	sqlite3_exec(mainview->db, tq, NULL, NULL, NULL);

	mainview->file_edited = FALSE;
	setBusy(mainview, 2);
	hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), GTK_STOCK_SAVE, _("Saved"));
}


void callback_checklist_toggled(GtkCellRendererToggle *cell_renderer, gchar *path, GtkWidget *source)
{
gtk_object_set_data(GTK_OBJECT(source), "edited", TRUE);

GtkTreeIter iter;
GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(source));
GtkTreePath *treepath = gtk_tree_path_new_from_string(path);
if(gtk_tree_model_get_iter(model, &iter, treepath))
	{
	gboolean val;
  gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, CHECKNODE_CHECKED, &val, -1);
  gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_CHECKED, !val, -1);
	}
gtk_tree_path_free(treepath);
}

void callback_checklist_edited(GtkCellRendererToggle *cell_renderer, gchar *arg1, gchar *arg2, GtkWidget *source)
{
gtk_object_set_data(GTK_OBJECT(source), "edited", TRUE);

GtkTreeIter iter;
GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(source));
GtkTreePath *treepath = gtk_tree_path_new_from_string(arg1);
if(gtk_tree_model_get_iter(model, &iter, treepath))
	{
  gtk_list_store_set(GTK_LIST_STORE(model), &iter, CHECKNODE_TEXT, arg2, -1);
	}
gtk_tree_path_free(treepath);
}

void callback_checklist_change(GtkTreeSelection *selection, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

g_signal_handlers_block_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_block_by_func(mainview->strikethru_tb, callback_fontstyle, mainview->strikethru_tb);
g_signal_handlers_block_by_func(mainview->check_tb, callback_fontstyle, mainview->check_tb);

gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), FALSE);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->strikethru_tb), FALSE);
gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->check_tb), FALSE);

GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
GList* l=gtk_tree_selection_get_selected_rows(selection, NULL);

gboolean gotit=FALSE;
 	
GList* cur=l;
while(cur)
	{
	GtkTreePath *path=cur->data;

	if (!gotit)
		{
		GtkTreeIter iter;
		if (gtk_tree_model_get_iter(model, &iter, path))
			{
			gint styletoset_weight;
			gboolean styletoset_strike;
			gboolean ischecked;

	  	gtk_tree_model_get(GTK_TREE_MODEL(model), &iter, CHECKNODE_BOLD, &styletoset_weight, CHECKNODE_STRIKE, &styletoset_strike, CHECKNODE_CHECKED, &ischecked, -1);
	  	if (styletoset_weight==PANGO_WEIGHT_BOLD) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->bold_tb), TRUE);
	  	if (styletoset_strike==TRUE) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->strikethru_tb), TRUE);
	  	if (ischecked==TRUE) gtk_toggle_tool_button_set_active(GTK_TOGGLE_TOOL_BUTTON(mainview->check_tb), TRUE);
			gotit=TRUE;
			}
		}
	gtk_tree_path_free(path);
	cur=cur->next;
	}

g_list_free(l);

g_signal_handlers_unblock_by_func(mainview->bold_tb, callback_fontstyle, mainview->bold_tb);
g_signal_handlers_unblock_by_func(mainview->strikethru_tb, callback_fontstyle, mainview->strikethru_tb);
g_signal_handlers_unblock_by_func(mainview->check_tb, callback_fontstyle, mainview->check_tb);
}

void callback_checklist_paste(MainView *mainview)
{
    g_assert(mainview != NULL && mainview->data != NULL);
    gchar **entries;
    gint length, i;
    
    GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
    GtkTreeIter toplevel;
    gchar *pasted_text = gtk_clipboard_wait_for_text(mainview->clipboard);
    
    entries = g_strsplit(pasted_text, "\n", 0);
    length = g_strv_length(entries);
    
    for (i=0; i<length; i++) {
        gtk_list_store_append(GTK_LIST_STORE(model), &toplevel);
        gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_CHECKED, FALSE, CHECKNODE_TEXT, entries[i], -1);
    }
    
    gtk_object_set_data(GTK_OBJECT(mainview->listview), "edited", TRUE);
    g_free(pasted_text);
    g_strfreev(entries);
}	

void callback_checklist_add(GtkAction *action, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
GtkTreeIter    toplevel;
gtk_list_store_append(GTK_LIST_STORE(model), &toplevel);

gtk_list_store_set(GTK_LIST_STORE(model), &toplevel, CHECKNODE_CHECKED, FALSE, CHECKNODE_TEXT, "", -1);
GtkTreePath *path = gtk_tree_model_get_path(model, &toplevel);
if (path)
	{
	gtk_tree_view_set_cursor(GTK_TREE_VIEW(mainview->listview), path, mainview->listtextcol, TRUE);
	gtk_tree_path_free(path);
	}

gtk_object_set_data(GTK_OBJECT(mainview->listview), "edited", TRUE);
}

void callback_checklist_delete(GtkAction *action, MainView *mainview)
{
g_assert(mainview != NULL && mainview->data != NULL);

if (gtk_tree_selection_count_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)))==0)
	{
	hildon_banner_show_information(GTK_WIDGET(mainview->data->main_view), NULL, _("Select items first"));
	return;
	}

GtkWidget *dialog, *label, *but_ok, *but_cancel;

dialog = gtk_dialog_new();

label = gtk_label_new(_("Delete selected items?"));

but_ok = gtk_button_new_with_label(_("Yes, Delete"));
but_cancel = gtk_button_new_with_label(_("Cancel"));

gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_ok);
gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->action_area), but_cancel);

gtk_object_set_user_data(GTK_OBJECT(dialog), mainview);

gtk_signal_connect_object(GTK_OBJECT(but_cancel), "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy), dialog);
g_signal_connect(G_OBJECT(but_ok), "clicked", G_CALLBACK(callback_checklist_delete_real), dialog);

gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), label);

gtk_widget_show_all(dialog);
gtk_window_set_modal(GTK_WINDOW(dialog), TRUE);
}

void callback_checklist_delete_real(GtkAction *action, gpointer data)
{
GtkWidget *dialog = data;
MainView *mainview = gtk_object_get_user_data(GTK_OBJECT(dialog));
gtk_widget_destroy(dialog);

GtkTreeModel *model=gtk_tree_view_get_model(GTK_TREE_VIEW(mainview->listview));
GList* l=gtk_tree_selection_get_selected_rows(gtk_tree_view_get_selection(GTK_TREE_VIEW(mainview->listview)), NULL);

GList* rowrefs=NULL;
GList* cur=l;
while(cur)
	{
	GtkTreePath *path=cur->data;

	GtkTreeIter iter;
	if (gtk_tree_model_get_iter(model, &iter, path))
		{
  	GtkTreeRowReference  *rowref = gtk_tree_row_reference_new(model, path);
		rowrefs=g_list_append(rowrefs, rowref);
		}
	gtk_tree_path_free(path);
	cur=cur->next;
	}
g_list_free(l);

g_object_ref(model);
gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->listview), NULL);

cur=rowrefs;
while(cur)
	{
	GtkTreeRowReference *rowref=cur->data;
	GtkTreePath *path= gtk_tree_row_reference_get_path(rowref);
	if (path)	
		{
		GtkTreeIter iter;
		if (gtk_tree_model_get_iter(model, &iter, path)) gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
		gtk_tree_path_free(path);
		}
	gtk_tree_row_reference_free(rowref);
	cur=cur->next;
	}
g_list_free(rowrefs);

gtk_tree_view_set_model(GTK_TREE_VIEW(mainview->listview), model);
g_object_unref(model);
gtk_object_set_data(GTK_OBJECT(mainview->listview), "edited", TRUE);
}

gboolean search_foreach(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter* iter, gpointer user_data)
{
    SearchResults *results = (SearchResults*)user_data;
    nodeData *data;
    int i;
    gtk_tree_model_get(model, iter, NODE_DATA, &data, -1);
    for (i=0; i<results->index; i++) {
        if (results->ids[i] == data->sql3id) {
            gtk_tree_store_set(GTK_TREE_STORE(model), iter, NODE_BGCOLOR, results->main_view->color_highlight, -1);
            return FALSE;
        }
    }
    gtk_tree_store_set(GTK_TREE_STORE(model), iter, NODE_BGCOLOR, NULL, -1);
    return FALSE;
}

void callback_search_activate(GtkWidget* widget, gpointer user_data)
{
    MainView* main_view = (MainView*)user_data;
    GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(main_view->treeview));

    sqlite3_stmt *stmt = NULL;
    const char *dum;
    char *search_str;

    SearchResults *results = g_new(SearchResults, 1);
    results->index = 0;
    results->current_size = SEARCH_RESULTS_ARRAY_SIZE;
    results->ids = g_malloc(sizeof(int)*results->current_size);
    results->main_view = main_view;

    /* Save data so our tables reflect the current UI content */
    saveCurrentData(main_view);

    /* Create a search string "%text%" for SQLite's "LIKE" keyword */
    search_str = gtk_entry_get_text(main_view->search_entry);
    if (strlen(search_str) > 0) {
        sqlite3_prepare(main_view->db, "SELECT nodeid FROM " checklisttable_tmpname " WHERE name LIKE ?1 UNION SELECT nodeid FROM " datatable_tmpname " WHERE name LIKE ?1 OR (bodytype=1 AND bodyblob LIKE ?1)", -1, &stmt, &dum);
    
        search_str = g_strdup_printf("%%%s%%", search_str);
        
        if (sqlite3_bind_text(stmt, 1, search_str, -1, SQLITE_TRANSIENT) != SQLITE_OK) {
            fprintf(stderr, "Error while binding search string to SQL query\n");
            return;
        }
 
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            if (results->index > results->current_size-1) {
                results->current_size += SEARCH_RESULTS_ARRAY_SIZE;
                results->ids = g_realloc(results->ids, sizeof(int)*results->current_size);
            }
            results->ids[results->index++] = sqlite3_column_int(stmt, 0);
        }
        fprintf(stderr, "Search for \"%s\" gave %d results.\n", search_str, results->index);
        g_free(search_str);

        sqlite3_finalize(stmt);
    }
    
    gtk_tree_model_foreach(model, search_foreach, results);
    
    g_free(results->ids);
    g_free(results);

    /* Give the treeview the focus, so the virtual keyboard hides (if any) */
    gtk_widget_grab_focus(GTK_WIDGET(main_view->treeview));
}

