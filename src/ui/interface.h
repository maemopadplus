/*
 * This file is part of maemopad+
 *
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef INTERFACE_H
#define INTERFACE_H

#include <gtk/gtk.h>
#include <appdata.h>
#include <sqlite3.h>
#include <ui/sketchwidget.h>
#include <config.h>

/**
 * If you want to have finger-friendly support (i.e. big fonts in
 * checklists, big scrollbars in checklists, texts and sketches),
 * uncomment the following #define line.
 **/
/* #define MAEMOPADPLUS_FINGER_FRIENDLY */
#define MAEMOPADPLUS_FINGER_FONT "sans 20"

#include <hildon/hildon-program.h> 
#include <hildon/hildon-color-button.h>
#include <wptextview.h>
#include <wptextbuffer.h>

#define NEW_SEL_LOGIC
#define EXPANDING_ROWS


#ifndef EXPANDING_ROWS
	#error EXPANDING_ROWS is needed for NEW_SEL_LOGIC
#endif

#define _(String) gettext(String)

#define USE_TEMP_TABLES

#ifdef USE_TEMP_TABLES
	#define TEMPTABLE_KEYWORD " TEMP"
#else
	#define TEMPTABLE_KEYWORD ""
#endif

/*
 * confirmation responses 
 */
#define CONFRESP_YES 1
#define CONFRESP_NO 2
#define CONFRESP_OK 3
#define CONFRESP_CANCEL 4
#define CONFRESP_ERROR 5

#define BRUSHSIZE_COUNT 7

#define SKETCHNODE_X 288
#define SKETCHNODE_Y 96
#define SKETCHNODE_RX 144
#define SKETCHNODE_RY 48

typedef enum
	{
		NODE_UNKNOWN = 0,
		NODE_TEXT,
		NODE_SKETCH,
		NODE_CHECKLIST
	} nodeType;

typedef enum
	{
		NODEFLAG_NONE = 0,
		NODEFLAG_SKETCHLINES = 1 << 0,
		NODEFLAG_SKETCHGRAPH = 1 << 1,
		NODEFLAG_WORDWRAP = 1 << 2
			/*
			 * NODEFLAG_OTHER = 1 << 3
			 * NODEFLAG_OTHER2 = 1 << 4
			 */
	} nodeFlag;

enum
	{
		NODE_NAME,
		NODE_PIXBUF,
		NODE_DATA,
                NODE_BGCOLOR,
		N_COLUMNS
	};

enum
	{
		CHECKNODE_CHECKED,
		CHECKNODE_TEXT,
		CHECKNODE_COLOR,
		CHECKNODE_BOLD,
		CHECKNODE_STRIKE,
		CHECKN_COLUMNS
	};

typedef enum
	{
		CHECKSTYLE_CHECKED = 1 << 0,
		CHECKSTYLE_BOLD = 1 << 1,
		CHECKSTYLE_STRIKE = 1 << 2
	} checklistStyle;

typedef struct _nodeData nodeData;
struct _nodeData
	{
		unsigned int sql3id;

		nodeType typ;
		gchar *name;
		GdkPixbuf *namepix;

		unsigned long lastMod;
		unsigned int flags;
	};




/*
 * Struct to include view's information 
 */
typedef struct _MainView MainView;
struct _MainView
	{
		/*
		 * Handle to app's data 
		 */
		AppData *data;

		guint viewflags;
		/*old (guint fullscreen): 0:normal 1:notree-normal 2:normal-full 3:notree-full 4:justdoc-full */
		/*new (guint viewflags): 1:tree 2:toolbar 4:fullscreen*/

		/*
		 * Items for menu 
		 */
		GtkWidget *file_item;
		GtkWidget *tools_item;
		GtkWidget *new_node_item;
		GtkWidget *delete_node_item;
		GtkWidget *rename_node_item;
		GtkWidget *expand_collapse_node_item;
		GtkWidget *export_node_item;
		GtkWidget *move_node_item;
		GtkWidget *move_up_node_item;
		GtkWidget *move_down_node_item;
		GtkWidget *move_to_bottom_node_item;
		GtkWidget *move_to_top_node_item;
		GtkWidget *new_item;
		GtkWidget *open_item;
		GtkWidget *save_item;
		GtkWidget *edit_item;
		GtkWidget *cut_item;
		GtkWidget *copy_item;
		GtkWidget *paste_item;
		GtkWidget *clear_item;
		GtkWidget *tools_brushsize;
		GtkWidget *tools_pagestyle;
		GtkWidget *tools_shape;
		GtkWidget *tools_font;
		GtkWidget *tools_wordwrap;
		GtkWidget *tools_color;
		GtkWidget *tools_pressure;
		GtkWidget *view_item;
		
		GtkWidget *brushsizemenu;
		GtkWidget *sketchlinesmenu;
		GtkWidget *viewmenu;
		GtkWidget *shapemenu;

		/*
		 * Toolbar 
		 */
		GtkWidget *toolbar;
		GtkWidget *iconw;

		GtkToolItem *toggletree_tb;

		GtkToolItem *new_tb;

		/*
		 * GtkToolItem* open_tb;
		 */
		GtkToolItem *save_tb;
		GtkToolItem *separator_tb1;

		/*
		 * GtkToolItem* cut_tb;
		 * GtkToolItem* copy_tb;
		 * GtkToolItem* paste_tb;
		 * GtkToolItem* separator_tb2;
		 */
		GtkToolItem *bold_tb, *italic_tb, *underline_tb, *bullet_tb, *strikethru_tb, *check_tb, *checkadd_tb, *checkdel_tb;

		GtkToolItem *font_tb;
		GtkWidget *colorbutton;
		GtkToolItem *colorbutton_tb;
		GtkWidget *viewmenuitems[3];

		GtkToolItem *brushsize_tb;
		GtkWidget *brushsizemenuitems[BRUSHSIZE_COUNT];

		GtkToolItem *eraser_tb;

		GtkToolItem *sketchlines_tb;
		GtkWidget *sketchlinesmenuitems[3];

		GtkToolItem *undo_tb;
		GtkToolItem *redo_tb;

		GtkToolItem *shape_tb;
		GtkWidget *shapemenuitems[4];
                
                GtkEntry *search_entry;

		/*
		 * Textview related 
		 */
		GtkWidget *scrolledwindow;	/* textview is under this widget */
		GtkWidget *textview;				/* widget that shows the text */
		WPTextBuffer *buffer;			/* buffer that contains the text */
		GtkClipboard *clipboard;		/* clipboard for copy/paste */

		GtkWidget *listscroll;	/* checklist is under this widget */
		GtkWidget *listview; /*checklist */
		GtkTreeViewColumn *listtextcol;

		GtkWidget *treeview;
		GtkWidget *scrolledtree;		/* scrolledwindow for the tree */
		GtkWidget *hpaned;					/* hpaned between tree and document */

		SketchWidget *sk;
		guint brushsize_backup;

		GdkCursor *cursorBusy;
		guint busyrefcount;

		gboolean file_edited;				/* for node operations and notification of new stuff in tmptable */
		gchar *file_name;
		gboolean loading;

                /* For highlighting searches */
                char* color_highlight;

		sqlite3 *db;

#ifdef NEW_SEL_LOGIC
		nodeData *cansel_node;
		guint32 cansel_time;
#endif

		gboolean newnodedialog_createchild;
	};


/* Initial size and increment for the search result node ID array */
#define SEARCH_RESULTS_ARRAY_SIZE 64

/* Saves the found node IDs after a search */
typedef struct _SearchResults SearchResults;
struct _SearchResults {
    int *ids;
    int current_size;
    int index;
    MainView *main_view;
};


#define datatable_name "nodes"
#define datatable_tmpname "tmpnodes"
#define datatable_backupname "nodesbackup"

#define datatable "( \
	nodeid INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
	parent INTEGER NOT NULL DEFAULT 0, \
	bodytype INTEGER NOT NULL DEFAULT 0, \
	name TEXT, \
	body TEXT, \
	nameblob BLOB, \
	bodyblob BLOB, \
	lastmodified INTEGER NOT NULL DEFAULT 0, \
	ord INTEGER NOT NULL DEFAULT 0, \
	flags INTEGER NOT NULL DEFAULT 0);"

#define dataindex "(parent, ord);"

#define misctable_name "settings"
#define misctable "(skey TEXT UNIQUE, sval TEXT);"

#define datatableversion 1

#define checklisttable_name "checklists"
#define checklisttable_tmpname "tmpchecklists"
#define checklisttable_backupname "checklistsbackup"

#define checklisttable "( \
	idx INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
	nodeid INTEGER NOT NULL DEFAULT 0, \
	name TEXT, \
	style INTEGER NOT NULL DEFAULT 0, \
	color INTEGER NOT NULL DEFAULT 0, \
	ord INTEGER NOT NULL DEFAULT 0);"
#define checklistindex "(nodeid, ord);"

#define checklisttableversion 1

/*
 * Publics: 
 */
void sk_set_brushsize(MainView * main, guint bsize);
void _toggle_tool_button_set_inconsistent(GtkToggleToolButton *button, gboolean inconsistent);
MainView *interface_main_view_new(AppData * data);
void interface_main_view_destroy(MainView * main);
gchar *interface_file_chooser(MainView * mainview, GtkFileChooserAction action, gchar * suggname, gchar * suggext);
gint interface_create_new_file(MainView * main);

#endif
