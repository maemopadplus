/*
 * This file is part of maemopad+
 *
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <ui/interface.h>
#include <gtk/gtk.h>
#include <appdata.h>

void prepareUIforNodeChange(MainView * mainview, nodeType typ);

nodeData *getSelectedNode(MainView * mainview);
void saveCurrentData(MainView * mainview);
void saveDataToNode(MainView * mainview, nodeData *selnode);

gboolean callback_treeview_testcollapse(GtkTreeView * treeview, GtkTreeIter * arg1, GtkTreePath * arg2, gpointer user_data);
void callback_treeview_celldatafunc(GtkTreeViewColumn * tree_column, GtkCellRenderer * cell, GtkTreeModel * tree_model, GtkTreeIter * iter, gpointer data);
void callback_treeview_change(GtkTreeSelection * selection, gpointer data);
gboolean treeview_canselect(GtkTreeSelection * selection, GtkTreeModel * model, GtkTreePath * path, gboolean path_currently_selected, gpointer userdata);

gboolean newnodedlg_key_press_cb(GtkWidget * widget, GdkEventKey * event, GtkWidget * dlg);
void add_new_node(nodeData * node, MainView * mainview, gboolean ischild);

void callback_file_new_text_node(GtkAction * action, gpointer data);
void callback_new_node_real(GtkAction * action, gpointer data);

void callback_file_new_node(GtkAction * action, gpointer data);

void callback_file_expand_collapse_node(GtkAction * action, gpointer data);
void callback_file_export_node(GtkAction * action, gpointer data);

void callback_file_delete_node(GtkAction * action, gpointer data);
void callback_delete_node_real(GtkAction * action, gpointer data);

void callback_file_rename_node (GtkAction *action, gpointer data);
void callback_rename_node_real (GtkAction *action, gpointer data);

void callback_about(GtkAction * action, gpointer data);

/*
 *  move node
 */
void callback_move_up_node(GtkAction * action, gpointer data);
void callback_move_down_node(GtkAction * action, gpointer data);
void callback_move_to_top_level_node(GtkAction * action, gpointer data);
void callback_move_to_bottom_level_node(GtkAction * action, gpointer data);

/*
 * edit-> cut/copy/paste 
 */
void callback_edit_clear(GtkAction * action, gpointer data);
void callback_edit_cut(GtkAction * action, gpointer data);
void callback_edit_copy(GtkAction * action, gpointer data);
void callback_edit_paste(GtkAction * action, gpointer data);
gint cb_popup(GtkWidget * widget, GdkEvent * event);


/*
 * search entry
 */
void callback_search_activate(GtkWidget* widget, gpointer user_data);

/*
 * file-> new/open/save 
 */
gboolean closefile(MainView * mainview);
gboolean callback_file_close(GtkAction * action, gpointer data);
void callback_file_new(GtkAction * action, gpointer data);
gboolean reset_ctree(GtkTreeModel * model, GtkTreePath * path, GtkTreeIter * iter, gpointer data);
void new_file(MainView * mainview);
void callback_file_open(GtkAction * action, gpointer data);
gboolean open_file(gchar * filename, MainView * mainview);
void callback_file_save(GtkAction * action, gpointer data);

/*
 * font/color 
 */
void callback_shapemenu(GtkAction * action, GtkWidget * wid);
void callback_eraser(GtkAction * action, MainView * mainview);
void callback_toggletree(GtkAction * action, MainView * mainview);
void callback_menu(GtkAction * action, GtkWidget * menu);
void callback_viewmenu(GtkAction * action, GtkWidget * wid);
void callback_brushsizetb(GtkAction * action, MainView *mainview);
void callback_brushsize(GtkAction * action, GtkWidget * wid);
void callback_sketchlines(GtkAction * action, GtkWidget * wid);
void callback_color(HildonColorButton * colorButton, MainView * mainview);
void callback_color_invoke(GtkAction * action, gpointer data);
void callback_pressure(GtkAction * action, MainView *mainview);
void callback_wordwrap(GtkAction * action, MainView *mainview);
void callback_font(GtkAction * action, gpointer data);
void callback_fontstyle(GtkAction * action, GtkWidget * wid);
void callback_textbuffer_move(WPTextBuffer *textbuffer, MainView *mainview);

gint wp_savecallback(const gchar *buffer, GString * gstr);

void callback_undo(GtkAction * action, MainView * mainview);
void callback_redo(GtkAction * action, MainView * mainview);

void callback_undotoggle(gpointer widget, gboolean st, MainView * mainview);
void callback_redotoggle(gpointer widget, gboolean st, MainView * mainview);
void callback_finger(SketchWidget * sk, gint x, gint y, gdouble pressure, MainView * mainview);

gboolean close_cb(GtkWidget * widget, GdkEventAny * event, MainView * mainview);
gboolean key_press_cb(GtkWidget * widget, GdkEventKey * event, MainView * mainview);

void callback_setview(MainView *mainview, int setmenu);

void callback_checklist_toggled(GtkCellRendererToggle *cell_renderer, gchar *path, GtkWidget *source);
void callback_checklist_edited(GtkCellRendererToggle *cell_renderer, gchar *arg1, gchar *arg2, GtkWidget *source);
void callback_checklist_change(GtkTreeSelection *selection, MainView *mainview);
void callback_checklist_paste(MainView *mainview);
void callback_checklist_add(GtkAction *action, MainView *mainview);
void callback_checklist_delete(GtkAction *action, MainView *mainview);
void callback_checklist_delete_real(GtkAction *action, gpointer data);


/*
 * buffer modified 
 */
void callback_buffer_modified(GtkAction * action, gpointer data);

void hw_event_handler(osso_hw_state_t * state, gpointer data);
void exit_event_handler(gboolean die_now, gpointer data);

#endif
