/*
 * This file is part of maemopad+
 *
 *
 * This software is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef MAEMOPAD_APPDATA_H
#define MAEMOPAD_APPDATA_H

#include <libosso.h>
#include <gconf/gconf-client.h>
#include <config.h>
#include <hildon/hildon-program.h>

typedef struct _AppData AppData;

struct _AppData
{
    HildonProgram  *app; /* handle to application */
    HildonWindow *main_view; /* handle to app's view */
    osso_context_t *osso; /* handle to osso */
    GConfClient* gconf;
};

#endif
